import sys
import pickle
import matplotlib.pyplot as plt
import numpy as np
import glob

data = []
labels = []
fig = plt.figure()
ax  = fig.add_subplot(111)

results = [
"/home/nseydoux/dev/edr/results/coopis/iter_12/scala/results_clone_f_0_rpi23sb",
"/home/nseydoux/dev/edr/results/coopis/iter_12/scala/results_clone_f_1_rpi23sb",
"/home/nseydoux/dev/edr/results/coopis/iter_12/scala/results_clone_f_2_rpi23sb"
]

index = 0

labels = []
transit = []
processing = []
idle = []

for path in results:
    # for method in ["adp", "cip", "cdp", "cdr", "cir"]:
    for method in ["adp", "cir"]:
        index += 1
        journeys = []
        with open(path+"/journeys_{0}.pkl".format(method), "rb") as f:
            journeys = pickle.load(f)

        method_transit = [j["transit"] for j in journeys if j["transit"] < 7000]
        method_idle = [j["idle"] for j in journeys if j["idle"] < 7000]
        method_processing = [j["processing"] for j in journeys if j["processing"] < 7000]

        width = 0.35       # the width of the bars: can also be len(x) sequence
        # transit_mean = [np.average(centralized_transit), np.average(distributed_transit)]
        # processing_mean = [np.average(centralized_processing), np.average(distributed_processing)]
        # idle_mean = [np.average(centralized_idle), np.average(distributed_idle)]

        labels.append(str(index)+"_"+method)
        # Absolute
        transit.append(np.average(method_transit))
        processing.append(np.average(method_processing))
        idle.append(np.average(method_idle))

# p1 = plt.bar(["C", "D"], transit_mean, width, color="blue")
# p2 = plt.bar(["C", "D"], processing_mean, width, bottom=transit_mean, color="yellow")
# p3 = plt.bar(["C", "D"], idle_mean, width, bottom=[x + y for x, y in zip(transit_mean, processing_mean)], color="red")

p1 = plt.bar(labels, transit, width, color="blue")
p2 = plt.bar(labels, processing, width, bottom=transit, color="yellow")
p3 = plt.bar(labels, idle, width, bottom=[x + y for x, y in zip(transit, processing)], color="red")

plt.ylabel('Durations (s)')
plt.xlabel('Topologies')
plt.title('Average repartition of delays')
# plt.yticks(np.arange(0, 81, 10))
plt.legend((p1[0], p2[0], p3[0]), ('Transit', 'Processing', 'Idle'))
plt.show()
