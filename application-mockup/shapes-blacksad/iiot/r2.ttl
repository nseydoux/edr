prefix san:   <http://www.irit.fr/recherches/MELODI/ontologies/SAN#>
prefix xsd:   <http://www.w3.org/2001/XMLSchema#>
prefix fn:    <http://w3id.org/sparql-generate/fn/>
prefix iter:  <http://w3id.org/sparql-generate/iter/>
prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
prefix adr:   <https://w3id.org/laas-iot/adream#>
prefix iotl:  <http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite#>
prefix ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
prefix ioto:  <http://www.irit.fr/recherches/MELODI/ontologies/IoT-O#>
prefix edr:   <http://w3id.org/laas-iot/edr#>
prefix ex:    <http://example.com/ns#>
prefix sh:    <http://www.w3.org/ns/shacl#>
prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix lmu:   <http://w3id.org/laas-iot/lmu#>
prefix dul:   <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
prefix time:  <http://www.w3.org/2006/time#>

ex:IIoTR2Envelope
	edr:hasTransferShape ex:IIoTR2TransferShape ;
	edr:hasApplyShape ex:IIoTR2ApplicableShape ;
	edr:hasDeliveryShape ex:IIoTR2ResultTransferShape ;
	edr:hasDeductionShape ex:IIoTR2ActiveShape.

ex:IIoTR2TransferShape
	a sh:NodeShape ;
	a edr:TransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this
				WHERE {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:producesDataOn adr:Presence, adr:Luminosity, adr:ConveyorState ;
							 	lmu:hasUpstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{ex:IIoTR2CoreRule edr:transferredTo $this.}
									UNION
									{ex:IIoTR2CoreRule edr:transferableTo $this.}
								}
						}
        }
        """ ;
  ].

ex:IIoTR2RuleTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR2TransferShape ;
		sh:construct """
			PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
			PREFIX ex:<http://example.com/ns#>
			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				ex:IIoTR2CoreRule edr:transferableTo $this.
				ex:IIoTR2CoreRule edr:transferredFrom ?host.
			} WHERE {
				$this lmu:hasUpstreamNode ?host.
				?host a lmu:HostNode.
			}
		""";
	].

ex:IIoTR2ApplicableShape
	a sh:NodeShape ;
	a edr:ApplicableShape ;
	a edr:NodeSensitiveComponent;
		sh:targetClass lmu:HostNode ;
	  sh:sparql [
	    sh:select """
	        PREFIX edr: <http://w3id.org/laas-iot/edr#>
	        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
	        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
	        prefix adr:   <https://w3id.org/laas-iot/adream#>
					prefix ex:    <http://example.com/ns#>
					PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
	        SELECT $this
					WHERE {
						FILTER NOT EXISTS {
							ex:IIoTR2CoreRule edr:isRuleActivable "true"^^xsd:boolean.
						}
						FILTER NOT EXISTS {
							$this a lmu:HostNode.
							$this lmu:hasDownstreamNode ?presenceProvider, ?luminosityProvider, ?stateProvider.
							?presenceProvider edr:producesDataOn adr:Presence.
							?luminosityProvider	edr:producesDataOn adr:Luminosity.
							?stateProvider edr:producesDataOn adr:ConveyorState.
							FILTER NOT EXISTS {
								ex:IIoTR2CoreRule edr:isRuleActive "true"^^xsd:boolean.
							}
							FILTER EXISTS {
								$this lmu:hasDownstreamNode ?lowerNode.
								FILTER(?lowerNode = ?luminosityProvider || ?lowerNode = ?presenceProvider || ?lowerNode = ?stateProvider)
								FILTER NOT EXISTS {
									?lowerNode edr:producesDataOn adr:Presence, adr:Luminosity, adr:ConveyorState.
								}
							}
						}
					}
	        """ ;
	  ].

ex:IIoTR2ApplicantRule
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR2ApplicableShape ;
		sh:construct """
			prefix adr:   <https://w3id.org/laas-iot/adream#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			prefix ex:    <http://example.com/ns#>
			prefix lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:isInterestedIn adr:Luminosity, adr:Presence, adr:ConveyorState.
				$this edr:producesDataOn ex:LowMachineVisibility.
				?partialDataProvider edr:partialDataProvider ?partialProduction.
				ex:IIoTR2CoreRule edr:isRuleActive "true"^^xsd:boolean.
				?originator edr:consumesResult ex:IIoTR2CoreRule.
			} WHERE {
				$this a lmu:HostNode.
				{
					$this lmu:hasDownstreamNode ?partialDataProvider.
					?partialDataProvider edr:producesDataOn ?partialProduction.
					FILTER NOT EXISTS {
						?partialDataProvider edr:producesDataOn adr:Luminosity, adr:Presence, adr:ConveyorState.
					}
				} UNION {
					ex:IIoTR2CoreRule edr:isRuleActivable "true"^^xsd:boolean.
				}

				ex:IIoTR2CoreRule edr:ruleOriginatedFrom ?originator.
				OPTIONAL{ex:IIoTR2CoreRule edr:isRuleActive "false"^^xsd:boolean.}
			}
		""";
	].

ex:IIoTR2ActiveShape
		a sh:NodeShape ;
		sh:targetClass lmu:HostNode ;
		a edr:ActiveShape ;
	  sh:sparql [
	    sh:select """
	        PREFIX edr: <http://w3id.org/laas-iot/edr#>
	        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
	        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
	        prefix adr:   <https://w3id.org/laas-iot/adream#>
					prefix ex:    <http://example.com/ns#>
					PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
	        SELECT $this
					WHERE {
						FILTER NOT EXISTS {
							$this a lmu:HostNode.
							ex:IIoTR2CoreRule edr:isRuleActive "true"^^xsd:boolean.
						}
	        }
	        """ ;
	  ].

ex:IIoTR2ResultTransferShape
	a sh:NodeShape ;
	a edr:ResultTransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this
				WHERE {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:isInterestedIn ex:LowConveyorVisibility ;
							 	lmu:hasDownstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{$this edr:consumesResult ex:IIoTR2CoreRule.}
								}
						}
        }
        """ ;
  ].

ex:IIoTR2ResultTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR2ResultTransferShape ;
		sh:construct """
			PREFIX ex:<http://example.com/ns#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:consumesResult ex:IIoTR2CoreRule.
			} WHERE {
				$this a lmu:Node ;
			}
		""";
	].

ex:myIIoTApp a lmu:Application ;
	iotl:exposes [
		iotl:endpoint "http://blacksad.laas.fr:8006";
	].

ex:IIoTR2RuleShape
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule ex:IIoTR2CoreRule .

ex:IIoTR2CoreRule
	a sh:SPARQLRule ;
	sh:condition ex:IIoTR2ActiveShape ;
	edr:ruleOriginatedFrom ex:myIIoTApp ;
	edr:originatingEndpoint "http://blacksad.laas.fr:8006" ;
	sh:construct """
		PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
		PREFIX ex:<http://example.com/ns#>
		PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
		PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
		PREFIX edr: <http://w3id.org/laas-iot/edr#>
		PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
		PREFIX iiot: <https://w3id.org/laas-iot/edr/iiot/factory#>
		CONSTRUCT {
			?deduction a rdf:Statement;
				rdf:subject ?feature;
				rdf:predicate rdf:type;
				rdf:object ex:LowConveyorVisibility;
				edr:deducedAt ?now;
				edr:deducedBy $this;
				edr:deducedWith ex:IIoTR2CoreRule;
				edr:deducedFrom ?presence_obs, ?luminosity_obs, ?state_obs.
		} WHERE {
			?presence rdf:type/rdfs:subClassOf* <https://w3id.org/laas-iot/adream#Presence>;
				ssn:isPropertyOf ?presenceFeature.
			?presence_obs ssn:observationResult/ssn:hasValue/dul:hasDataValue ?presenceValue;
				ssn:observedProperty ?presence;
				ssn:observedBy ?presence_sensor.
			FILTER(?presenceValue = '1.0'^^xsd:float)

			?luminosity rdf:type/rdfs:subClassOf* <https://w3id.org/laas-iot/adream#Luminosity>;
				ssn:isPropertyOf ?feature.
			?luminosity_obs	ssn:observationResult/ssn:hasValue/dul:hasDataValue ?luminosityValue;
				ssn:observedProperty ?luminosity;
				ssn:observedBy ?luminosity_sensor.
			FILTER(?luminosityValue < '300.0'^^xsd:float)

			?state rdf:type/rdfs:subClassOf* <https://w3id.org/laas-iot/adream#ConveyorState>;
				ssn:isPropertyOf ?machine.
			?state_obs	ssn:observationResult/ssn:hasValue/dul:hasDataValue ?stateValue;
				ssn:observedProperty ?state;
				ssn:observedBy ?state_sensor.
			FILTER(?stateValue = '1.0'^^xsd:float)

			?machine a iiot:Conveyor;
				iiot:locatedIn? ?presenceFeature.
			?presenceFeature iiot:locatedIn? ?feature.

			$this a lmu:HostNode.

			FILTER NOT EXISTS {
				?presence_obs edr:usedForDeductionBy ex:IIoTR2CoreRule.
				?luminosity_obs edr:usedForDeductionBy ex:IIoTR2CoreRule.
				?state_obs edr:usedForDeductionBy ex:IIoTR2CoreRule.
			}
			FILTER NOT EXISTS {
				?otherDeduction edr:deducedWith ex:IIoTR2CoreRule;
					edr:deducedFrom ?presence_obs, ?luminosity_obs, ?state_obs.
			}
			BIND(URI(CONCAT('http://example.com/ns#comfortableSpotdeduction', STRUUID())) AS ?deduction )
			BIND(NOW() AS ?now)
		}
	""".
ex:LowConveyorVisibility rdfs:subClassOf <http://purl.oclc.org/NET/ssnx/ssn#Property>.
