prefix san:   <http://www.irit.fr/recherches/MELODI/ontologies/SAN#>
prefix xsd:   <http://www.w3.org/2001/XMLSchema#>
prefix fn:    <http://w3id.org/sparql-generate/fn/>
prefix iter:  <http://w3id.org/sparql-generate/iter/>
prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
prefix adr:   <https://w3id.org/laas-iot/adream#>
prefix iotl:  <http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite#>
prefix ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
prefix ioto:  <http://www.irit.fr/recherches/MELODI/ontologies/IoT-O#>
prefix edr:   <http://w3id.org/laas-iot/edr#>
prefix ex:    <http://example.com/ns#>
prefix sh:    <http://www.w3.org/ns/shacl#>
prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix lmu:   <http://w3id.org/laas-iot/lmu#>
prefix dul:   <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
prefix time:  <http://www.w3.org/2006/time#>

ex:IIoTR7Envelope
	edr:hasTransferShape ex:IIoTR7TransferShape ;
	edr:hasApplyShape ex:IIoTR7ApplicableShape ;
	edr:hasDeliveryShape ex:IIoTR7ResultTransferShape ;
	edr:hasDeductionShape ex:IIoTR7ActiveShape.

ex:IIoTR7TransferShape
	a sh:NodeShape ;
	a edr:TransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this
				WHERE {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:producesDataOn adr:MachineState, adr:ProductQuality ;
							 	lmu:hasUpstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{ex:IIoTR7CoreRule edr:transferredTo $this.}
									UNION
									{ex:IIoTR7CoreRule edr:transferableTo $this.}
								}
						}
        }
        """ ;
  ].

ex:IIoTR7RuleTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR7TransferShape ;
		sh:construct """
			PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
			PREFIX ex:<http://example.com/ns#>
			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				ex:IIoTR7CoreRule edr:transferableTo $this.
				ex:IIoTR7CoreRule edr:transferredFrom ?host.
			} WHERE {
				$this lmu:hasUpstreamNode ?host.
				?host a lmu:HostNode.
			}
		""";
	].

ex:IIoTR7ApplicableShape
	a sh:NodeShape ;
	a edr:ApplicableShape ;
	a edr:NodeSensitiveComponent;
		sh:targetClass lmu:HostNode ;
	  sh:sparql [
	    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>
				PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT $this
				WHERE {
					FILTER NOT EXISTS {
						ex:IIoTR7CoreRule edr:isRuleActivable "true"^^xsd:boolean.
					}
					FILTER NOT EXISTS {
						$this a lmu:HostNode.
						$this lmu:hasDownstreamNode ?machineStateProvider, ?qualityProvider.
						?machineStateProvider	edr:producesDataOn adr:MachineState.
						?qualityProvider	edr:producesDataOn adr:ProductQuality.
						FILTER NOT EXISTS {
							ex:IIoTR7CoreRule edr:isRuleActive "true"^^xsd:boolean.
						}
						FILTER EXISTS {
							$this lmu:hasDownstreamNode ?lowerNode.
							FILTER(?lowerNode = ?qualityProvider || ?lowerNode = ?machineSpeedProvider)
							FILTER NOT EXISTS {
								?lowerNode edr:producesDataOn adr:MachineState, adr:ProductQuality.
							}
						}
					}
				}
        """ ;].

ex:IIoTR7ApplicantRule
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR7ApplicableShape ;
		sh:construct """
			prefix adr:   <https://w3id.org/laas-iot/adream#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			prefix ex:    <http://example.com/ns#>
			prefix lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:isInterestedIn adr:MachineState, adr:ProductQuality.
				$this edr:producesDataOn ex:DefectiveManufacturingUnit.
				?partialDataProvider edr:partialDataProvider ?partialProduction.
				ex:IIoTR7CoreRule edr:isRuleActive "true"^^xsd:boolean.
			} WHERE {
				$this a lmu:HostNode.
				{
					$this lmu:hasDownstreamNode ?partialDataProvider.
					?partialDataProvider edr:producesDataOn ?partialProduction.
					FILTER NOT EXISTS {
						?partialDataProvider edr:producesDataOn adr:MachineState, adr:ProductQuality.
					}
				} UNION {
					ex:IIoTR7CoreRule edr:isRuleActivable "true"^^xsd:boolean.
				}
				OPTIONAL{ex:IIoTR7CoreRule edr:isRuleActive "false"^^xsd:boolean.}
			}
		""";
	].

ex:IIoTR7ActiveShape
		a sh:NodeShape ;
		sh:targetClass lmu:HostNode ;
		a edr:ActiveShape ;
	  sh:sparql [
	    sh:select """
	        PREFIX edr: <http://w3id.org/laas-iot/edr#>
	        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
	        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
	        prefix adr:   <https://w3id.org/laas-iot/adream#>
					prefix ex:    <http://example.com/ns#>
					PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
	        SELECT $this
					WHERE {
						FILTER NOT EXISTS {
							$this a lmu:HostNode.
							ex:IIoTR7CoreRule edr:isRuleActive "true"^^xsd:boolean.
						}
	        }
	        """ ;
	  ].

ex:IIoTR7ResultTransferShape
	a sh:NodeShape ;
	a edr:ResultTransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this
				WHERE {
						FILTER NOT EXISTS {
							{
								$this a lmu:Node ;
									edr:isInterestedIn ex:DefectiveManufacturingUnit ;
								 	lmu:hasDownstreamNode [
										a lmu:HostNode;
									].
							} UNION {
								ex:IIoTR7CoreRule edr:ruleOriginatedFrom $this.
							}
							FILTER NOT EXISTS {
								{$this edr:consumesResult ex:IIoTR7CoreRule.}
							}
						}
        }
        """ ;
  ].

ex:IIoTR7ResultTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:IIoTR7ResultTransferShape ;
		sh:construct """
			PREFIX ex:<http://example.com/ns#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:consumesResult ex:IIoTR7CoreRule.
			} WHERE {
				$this a lmu:Node ;
			}
		""";
	].

ex:myIIoTApp a lmu:Application, lmu:Node ;
	iotl:exposes [
		iotl:endpoint "http://localhost:8006";
	].

ex:IIoTR7RuleShape
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule ex:IIoTR7CoreRule .

ex:IIoTR7CoreRule
	a sh:SPARQLRule ;
	sh:condition ex:IIoTR7ActiveShape ;
	edr:ruleOriginatedFrom ex:myIIoTApp ;
	edr:originatingEndpoint "http://localhost:8006" ;
	sh:construct """
		PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
		PREFIX ex:<http://example.com/ns#>
		PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
		PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
		PREFIX edr: <http://w3id.org/laas-iot/edr#>
		PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
		PREFIX iiot: <https://w3id.org/laas-iot/edr/iiot/factory#>
		CONSTRUCT {
			?deduction a rdf:Statement;
				rdf:subject ?machine;
				rdf:predicate rdf:type;
				rdf:object ex:DefectiveManufacturingUnit;
				edr:deducedAt ?now;
				edr:deducedBy $this;
				edr:deducedWith ex:IIoTR7CoreRule;
				edr:deducedFrom ?machineState_obs, ?productQuality_obs.
		} WHERE {

			?machineState rdf:type/rdfs:subClassOf* <https://w3id.org/laas-iot/adream#MachineState>;
				ssn:isPropertyOf ?machine.
			?machineState_obs	ssn:observationResult/ssn:hasValue/dul:hasDataValue ?machineStateValue;
				ssn:observedProperty ?machineState;
				ssn:observedBy ?machineState_sensor.
			FILTER(?machineStateValue = '1.0'^^xsd:float)

      ?productQuality rdf:type/rdfs:subClassOf* <https://w3id.org/laas-iot/adream#ProductQuality>;
				ssn:isPropertyOf ?machine.
			?productQuality_obs	ssn:observationResult/ssn:hasValue/dul:hasDataValue ?productQualityValue;
				ssn:observedProperty ?productQuality;
				ssn:observedBy ?productQuality_sensor.
			FILTER(?productQualityValue < '98.0'^^xsd:float)

			$this a lmu:HostNode.

			FILTER NOT EXISTS {
				?productQuality_obs edr:usedForDeductionBy ex:IIoTR7CoreRule.
				?machineState_obs edr:usedForDeductionBy ex:IIoTR7CoreRule.
			}

			FILTER NOT EXISTS {
				?otherDeduction edr:deducedWith ex:IIoTR7CoreRule;
					edr:deducedFrom ?machineState_obs, ?productQuality_obs.
			}
			BIND(URI(CONCAT('http://example.com/ns#comfortableSpotdeduction', STRUUID())) AS ?deduction )
			BIND(NOW() AS ?now)
		}
	""".
ex:DefectiveManufacturingUnit rdfs:subClassOf <http://purl.oclc.org/NET/ssnx/ssn#Property>.
