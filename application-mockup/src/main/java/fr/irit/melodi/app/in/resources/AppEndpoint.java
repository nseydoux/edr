package fr.irit.melodi.app.in.resources;

import java.net.HttpURLConnection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.app.Controller;
import fr.irit.melodi.app.model.AppModel;

@Path("/")
public class AppEndpoint {
	Logger LOGGER = LogManager.getLogger(AppEndpoint.class);

	@POST
	@Path("/notify")
	public Response register(String listeningEndpoint, @Context org.glassfish.grizzly.http.server.Request httpRequest){
		LOGGER.info(httpRequest.getAttributeNames());
		LOGGER.info(httpRequest.getRemoteHost()+" "+httpRequest.getRemotePort()+" "+httpRequest.getRemoteAddr());
		return Response.status(HttpURLConnection.HTTP_NO_CONTENT).build();
	}
	
	@POST
	@Path("/data/enriched")
	@Consumes("text/turtle")
	public Response addData(String data, @Context org.glassfish.grizzly.http.server.Request httpRequest){
//		LOGGER.info(httpRequest.getAttributeNames());
//		LOGGER.info(httpRequest.getRemoteHost()+" "+httpRequest.getRemotePort()+" "+httpRequest.getRemoteAddr());
		Controller.log(data);
		return Response.status(HttpURLConnection.HTTP_OK).build();
	}
}
