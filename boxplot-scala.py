import sys
import pickle
import matplotlib.pyplot as plt
import glob
import re

data = []
labels = []
fig = plt.figure()
ax  = fig.add_subplot(111)

results = [
    "/home/nseydoux/dev/edr/results/coopis/iter_11/specialisation_baseline/results_specialisation_baseline_3",
    "/home/nseydoux/dev/edr/results/coopis/iter_11/specialisation_implementation/results_specialisation_implementation_3"
    # "results/coopis/iter_12/distribution/results_distribution_0",
    # "results/coopis/iter_12/distribution/results_distribution_3"
 # "results/coopis/iter_9/specialisation_baseline_reproduce/results_specialisation_baseline_1",
 # "results/coopis/iter_9/specialisation_implementation_reproduce/results_specialisation_implementation_4"
 # "results/coopis/iter_11/specialisation_baseline/results_specialisation_baseline_3",
 # "results/coopis/iter_11/specialisation_implementation/results_specialisation_implementation_3"
 # "results/coopis/iter_11/distribution/results_distribution_0_1",
 # "results/coopis/iter_11/distribution/results_distribution_1_1",
 # "results/coopis/iter_11/distribution/results_distribution_2_1"
 ]

for method in ["cir", "cdr", "cdp", "cip", "adp"]:
# for method in ["cir", "cdr"]:
    for path in results:
        centralized = []
        print("Opening "+path+"/journ_delays_{0}".format(method))
        for data_file in glob.glob(path+"/journ_delays_{0}".format(method)):
            with open(data_file, "rb") as f:
                centralized.append(pickle.load(f))
        data.append([x for l in centralized for x in l if x<3000.0])
        # lbl = path.split("/")[5]
        # lbl = "s"+"0"
        labels.append(method)


plt.boxplot(data,0, '')
# plt.boxplot(data)
ax.set_ylabel('Delay (s)')
# ax.set_xlabel('Topologies and strategies')
# ax.set_xticklabels(labels, fontsize=14)
ax.set_xticklabels(labels, rotation=30, fontsize=8)
plt.show()
