import glob
import json
import argparse


def prepare_trace(logs_folder_path):
    trace=[]
    for tracefile_path in glob.glob(logs_folder_path+"/*.trace"):
        with open(tracefile_path, "r") as tracefile:
            for line in tracefile:
                event = json.loads(line)
                trace.append(event)
    trace.sort(key=lambda x: x["time"])
    return trace

def count_messages(trace):
    counter = 0
    for event in trace:
        if event["event"] == "Sending data":
            counter+=1
    return counter

parser = argparse.ArgumentParser(description='Processes the traces of an edr execution.')
parser.add_argument('--adp', type=str)
parser.add_argument('--cir', type=str)
parser.add_argument('--cdp', type=str)
parser.add_argument('--cdr', type=str)
parser.add_argument('--cip', type=str)
parser.add_argument('-o', '--output', help="the folder to which results are sent", type=str)

args = parser.parse_args()

for method, folder in {"adp":args.adp,
        "cdp":args.cdp,
        "cip":args.cip,
        "cdr":args.cdr,
        "cir":args.cir}.items():
    print("Counting messages for "+method)
    trace = prepare_trace(folder)
    with open(args.output+"/messages_{0}.txt".format(method), "w") as f:
        f.write(str(count_messages(trace)))
    del(trace)
