import glob
from subprocess import Popen
import subprocess
import sys
import os
import os.path
import time
import re
import argparse

def get_process_id(config_path):
    #if(re.match(".*\d{3}\.\d{3}\.\d+\.\d+.*", config_path) != None):
    if len(config_path.split("/")) > 4:
        return config_path.split("/")[5]
    else:
        return config_path.split("/")[3]

parser = argparse.ArgumentParser(description='Starts an edr simulation')
parser.add_argument('-t', '--topology', help="The folder containing the generated topology", type=str)
parser.add_argument('-a', '--applications', help="the folder containing the applications, or no-app", type=str)
parser.add_argument('-o', '--output', help="The folder to which results are sent", type=str)
#parser.add_argument('-d', '--data', help="Data to be replayed by simulated sensors", type=str)
parser.add_argument('-r', '--rate', help="Rate of active sensors (one in r)", type=str)
parser.add_argument('-d', '--duration', help="Time between the start of applications and the end of the simulation", type=int)

args = parser.parse_args()
SIM_TIME = 930
if args.duration != None:
    SIM_TIME = args.duration
    print(SIM_TIME)

# if len(sys.argv) != 3:
#     exit("usage : "+sys.argv[0]+" <topology folder> <applications folder | no-app>")

if args.topology == None or args.applications == None or args.output == None:
    exit(args.help)

topo_folder = args.topology
app_folder = args.applications
out_folder = args.output

if not os.path.exists(out_folder):
    os.makedirs(out_folder)

processes = []

for log in glob.glob(out_folder+"/*"):
    os.remove(log)

############ STARTING SENSORS
#for sensor in glob.glob(topo_folder+'sensor*'):
with open(out_folder+"/sensors.log", "w") as logfile:
    cmd = ["java", "-jar", "sensor.jar", topo_folder+'sensor*.json']
    if args.rate != None:
        # To apply the sensor rate, only one in rate sensors are left with the right pattern
        sensors = glob.glob(topo_folder+'sensor*.json')
        sensors.sort()
        index = 0
        for sensor in sensors:
            if index % int(args.rate) != 0:
                os.rename(sensor, os.path.dirname(sensor)+"/"+os.path.basename(sensor)[1:])
            index += 1
    processes.append(Popen(cmd, stdout=logfile, stderr=logfile))

print("Sensors started")


########### STARTING NODES
os.chdir("node")

buildings = glob.glob("../"+topo_folder+'building*')
buildings.sort()
for building in buildings:
    print("Launching "+building)
    with open("../"+out_folder+"/"+get_process_id(building)+".log", "w") as logfile:
        processes.append(Popen(["mvn", "exec:java", "-Dconfig="+building, "-Dfilename=../"+out_folder+"/"+get_process_id(building)+".ttl",  "-Dtracename=../"+out_folder+"/"+get_process_id(building)+".trace"], stdout=logfile, stderr=logfile))
        time.sleep(1)
    print("Done")
time.sleep(3)

floors = glob.glob("../"+topo_folder+'floor*')
floors.sort()
for floor in floors:
    print("Launching "+floor)
    with open("../"+out_folder+"/"+get_process_id(floor)+".log", "w") as logfile:
        processes.append(Popen(["mvn", "exec:java", "-Dconfig="+floor, "-Dfilename=../"+out_folder+"/"+get_process_id(floor)+".ttl", "-Dtracename=../"+out_folder+"/"+get_process_id(floor)+".trace"], stdout=logfile, stderr=logfile))
        time.sleep(1)
    print("Done")
time.sleep(3)

galleries = glob.glob("../"+topo_folder+'gallery*')
galleries.sort()
for gallery in galleries:
    print("Launching "+gallery)
    with open("../"+out_folder+"/"+get_process_id(gallery)+".log", "w") as logfile:
        processes.append(Popen(["mvn", "exec:java", "-Dconfig="+gallery, "-Dfilename=../"+out_folder+"/"+get_process_id(gallery)+".ttl", "-Dtracename=../"+out_folder+"/"+get_process_id(gallery)+".trace"], stdout=logfile, stderr=logfile))
        time.sleep(1)
    print("Done")
time.sleep(3)

rooms = glob.glob("../"+topo_folder+'room*')
rooms.sort()
for room in rooms:
    print("Launching "+room)
    with open("../"+out_folder+"/"+get_process_id(room)+".log", "w") as logfile:
        processes.append(Popen(["mvn", "exec:java", "-Dconfig="+room, "-Dfilename=../"+out_folder+"/"+get_process_id(room)+".ttl", "-Dtracename=../"+out_folder+"/"+get_process_id(room)+".trace"], stdout=logfile, stderr=logfile))
        time.sleep(1)
    print("Done")

####################### WAITING FOR NODES TO START...
time.sleep(2*len(processes))
#time.sleep(45)
####################### RESTORING SENSOR NAMES...
os.chdir("../")
sensors = glob.glob(topo_folder+'ensor*.json')
for sensor in sensors:
    os.rename(sensor, os.path.dirname(sensor)+"/s"+os.path.basename(sensor))

###################### ... AND LAUNCHING THE APPLICATIONS
if app_folder != "no-app":
    os.chdir("application-mockup")
    for application in glob.glob("../"+app_folder+'*.json'):
        time.sleep(2)
        print("Launching "+application)
        with open("../"+out_folder+"/"+application.split("/")[3]+".log", "w") as logfile:
            processes.append(Popen(["mvn", "exec:java", "-Dconfig="+application, "-Dfilename=../"+out_folder+"/"+application.split("/")[3]+".ttl"], stdout=logfile, stderr=logfile))
        print("Done")
    # os.chdir("../node")

time.sleep(SIM_TIME)

for p in processes:
    p.terminate()
