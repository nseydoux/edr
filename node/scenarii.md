# Scenarios

## Simple interest
- The master is created, as well as the bedroom and living room node
- The master receives an interest from an application
    + ex:myApp edr:isInterestedIn ex:Temperature.
- The master forwards its interest
-  The nodes redirect the measures they receive to the master

## Simple rule
- The master is created, as well as the bedroom and living room node
- The master receives a rule from an application
- the masters finds the interests of the rule
- the remaining is similar to the previous scenario