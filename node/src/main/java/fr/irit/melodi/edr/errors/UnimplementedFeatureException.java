package fr.irit.melodi.edr.errors;

public class UnimplementedFeatureException extends Exception {
	private static final long serialVersionUID = -5330248399909835154L;
	
	public UnimplementedFeatureException(String message) {
		super(message);
	}
}
