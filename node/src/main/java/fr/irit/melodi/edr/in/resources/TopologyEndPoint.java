package fr.irit.melodi.edr.in.resources;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.irit.melodi.edr.errors.UnimplementedFeatureException;
import fr.irit.melodi.edr.node.Node;

@Path("/topology")
public class TopologyEndPoint {
	private static final Logger LOGGER = LogManager.getLogger(TopologyEndPoint.class);
	
	@GET
	@Path("/ping")
	public Response ping(){
		return Response
			.status(HttpURLConnection.HTTP_OK)
			.entity("pong")
			.build();
	}
	
	@GET
	@Path("/presentation")
	public Response present(){
		return Response
				.status(HttpURLConnection.HTTP_OK)
				.entity(Node.getInstance().getKBManager().getAnnouncement(true))
				.build();
	}
	
	@GET
	@Path("/interests")
	public Response getInterests(){
		ObjectMapper om = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
			om.writeValue(sw, Node.getInstance().getInterestManager().getNodeInterests());
			return Response
					.status(HttpURLConnection.HTTP_OK)
					.entity(sw.toString())
					.build();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Response
				.status(500)
				.build();
	}
	
	private static String processAnnounce(String profile) throws UnimplementedFeatureException{
		Model m = ModelFactory.createDefaultModel();
		InputStream stream = new ByteArrayInputStream(profile.getBytes(StandardCharsets.UTF_8));
		m.read(stream, "http://example.com/ns#", "TTL");
		Node.getInstance().processUpstreamMessage(m);
		return Node.getInstance().getTopologyManager().extractNodeFromAnnounce(m);
	}
	
	/**
	 * Method called by a downstream node
	 * @param profile a ttl serialization of the annoucee profile
	 * @return the uri of the target node
	 */
	@POST
	@Path("/announce/downstream")
	@Produces(MediaType.TEXT_PLAIN)
	public Response downstreamAnnounce(String profile){
		LOGGER.debug(Node.getInstance().getName()+" received remote announce from downsream");
		LOGGER.trace(profile);
		try {
			String remote = processAnnounce(profile);
			if(remote != null){
				LOGGER.debug("Annouced node has uri "+remote);
				// TODO : The downstream node gets two messages where one would only be necessary
				// first, the update of the rules, and then the announce
				Node.getInstance().addDownstreamNode(profile);
				LOGGER.trace("Announced content added to "+Node.getInstance().getName()+" KB");
			}
			Model annouce = Node.getInstance().getKBManager().getAnnounceModel(false);
			if(annouce != null){
				LOGGER.debug("Responding to annouce from "+remote);
				StringWriter sw = new StringWriter();
				annouce.write(sw, "TTL");
				return  Response.status(HttpURLConnection.HTTP_OK)
		                .entity(sw.toString()).build();
			} else {
				return Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
	                .entity("Malformed RDF").build();
		}
	}

	/**
	 * Method called by an upstream node
	 * @param profile a ttl serialization of the annoucee profile
	 * @return the uri of the target node
	 */
	@POST
	@Path("/announce/upstream")
	@Produces(MediaType.TEXT_PLAIN)
	public Response upstreamAnnounce(String profile){
		LOGGER.debug(Node.getInstance().getName()+" received remote announce from upstream");
		LOGGER.trace(profile);
		try {
			String remote = processAnnounce(profile);
			if(remote != null){
				LOGGER.debug("Annouced node has uri "+remote);
				Node.getInstance().getTopologyManager().addUpstreamRemote(remote);
			}
			LOGGER.trace("Announced content added to "+Node.getInstance().getName()+" KB");
			return  Response.status(HttpURLConnection.HTTP_OK)
	                .entity(Node.getInstance().getUri()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
	                .entity("Malformed RDF").build();
		}
	}
}
