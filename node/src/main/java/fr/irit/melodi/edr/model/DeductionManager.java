package fr.irit.melodi.edr.model;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.edr.node.Tracer;
import fr.irit.melodi.edr.out.resources.DataClient;
import fr.irit.melodi.sparql.exceptions.IncompleteSubstitutionException;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;
import fr.irit.tools.queries.QueryEngine;

public class DeductionManager {
	private static final Logger LOGGER = LogManager.getLogger(RuleManager.class);
	private FolderManager queries;
	
	public DeductionManager(){
		try {
			this.queries = new FolderManager("queries/deductions");
			this.queries.loadQueries();
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}
	}
	
	public List<Map<String, String>> listDeductions(Model inference){
		String query = this.queries.getQueries().get("get_deductions");
		return QueryEngine.selectQuery(query, inference);
	}
	
	public Model describeDeduction(String uri, Model inference){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("deduction", uri);
		String query = "";
		try {
			query = this.queries.getTemplateQueries().get("describe_deduction").substitute(substitution);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return QueryEngine.describeQuery(query, inference);
	}
	
	public boolean containsDeductions(Model inference){
		String query = this.queries.getQueries().get("contains_deductions");
		return QueryEngine.askQuery(query, inference);
	}
	
	public List<Map<String, String>> getDeductionType(Model deduction){
		LOGGER.debug("Extracting type from deduction");
		LOGGER.debug(KBManager.serializeModel(deduction));
		String query = this.queries.getQueries().get("get_deductions_type");
		LOGGER.debug(query);
		List<Map<String, String>> results = QueryEngine.selectQuery(query, deduction);
		if(results.size()>1){
			LOGGER.error("More than one deduction was sent in a single message from downstream node");
		}
		return results;
	}
}
