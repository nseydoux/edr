package fr.irit.melodi.edr.model;

import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.Lock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.sparql.exceptions.IncompleteSubstitutionException;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;
import fr.irit.tools.queries.QueryEngine;
import fr.irit.tools.utils.ModelLoader;
import org.topbraid.shacl.rules.RuleUtil;
import org.topbraid.shacl.util.ModelPrinter;
import org.topbraid.shacl.validation.ValidationUtil;

public class KBManager {
	private static final Logger LOGGER = LogManager.getLogger(KBManager.class);
	
	private Model m;
	public static Set<Entry<String, String>> prefixes;
	private Set<String> loadedModels;
	private FolderManager queries;
	
	static{
		prefixes = new HashSet<Entry<String, String>>();
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("sh", "<http://www.w3.org/ns/shacl#>"));
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("ex", "<http://example.com/ns#>"));
	}
	
	public KBManager(){
		this.m = ModelFactory.createDefaultModel();
		this.loadedModels = new HashSet<>();
	}
	
	public KBManager(String... uris){
		this.m = ModelLoader.getLoadedModel(uris);
		this.loadedModels = new HashSet<>();
		for(String model : uris){
			this.loadedModels.add(model);
		}
	}
	
	public FolderManager getFolderManager(){
		return this.queries;
	}
	
	public void loadModel(String uri){
		if(!this.loadedModels.contains(uri)){
			this.m.enterCriticalSection(Lock.WRITE);
			try {
				if(uri.endsWith(".ttl")){
					this.m.read(uri, "TTL");
				} else {
					this.m.read(uri);
				}
			} finally {
				this.m.leaveCriticalSection();
			}
			this.loadedModels.add(uri);
		}
	}
	
	public void setQueryFolder(String path){
		try {
			this.queries = new FolderManager(path);
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}
		this.queries.loadQueries();
	}
	
	public List<String> getSensorsFromObs(Model m){
		List<Map<String, String>> results = QueryEngine.selectQuery(this.queries.getQueries().get("get_observation_sensor"), m);
		List<String> sensorsURI = new ArrayList<>();
		for(Map<String, String> result : results){
			sensorsURI.add(result.get("sensor"));
		}
		return sensorsURI;
	}
	
	public List<String> getNodeInterests(String nodeURI){
		List<String> interests = new ArrayList<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("node", Node.getInstance().getUri());
		
		try {
			for(Map<String, String> result : this.queryModel(this.queries.getTemplateQueries().get("get_node_interests").substitute(substitution))){
				interests.add(result.get("interest"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return interests;
	}
	
	public Model constructModel(String query){
		Model result = null;
		this.m.enterCriticalSection(Lock.READ);
		try {
			result = QueryEngine.constructQuery(query, this.m);
		} finally {
			this.m.leaveCriticalSection();
		}
		return result;
	}
	
	public List<Map<String, String>> queryModel(String query){
		List<Map<String, String>> result = null;
		this.m.enterCriticalSection(Lock.READ);
		try {
			result = QueryEngine.selectQuery(query, this.m);
		} finally {
			this.m.leaveCriticalSection();
		}
		return result;
	}
	
	public Boolean askModel(String query){
		Boolean result = null;
		this.m.enterCriticalSection(Lock.READ);
		try {
			result = QueryEngine.askQuery(query, this.m);
		} finally {
			this.m.leaveCriticalSection();
		}
		return result;
	}
	
	public Model describeModel(String query){
		Model result = null;
		this.m.enterCriticalSection(Lock.READ);
		try {
			result = QueryEngine.describeQuery(query, this.m);
		} finally {
			this.m.leaveCriticalSection();
		}
		return result;
	}
	
	public void updateModel(String query){
		this.m.enterCriticalSection(Lock.WRITE);
		try {
			QueryEngine.updateQuery(query, this.m);
		} finally {
			this.m.leaveCriticalSection();
		}
	}
	
	public void addToModel(Model model){
		this.m.enterCriticalSection(Lock.WRITE);
		try {
			this.m.add(model);
		} finally {
			this.m.leaveCriticalSection();
		}
	}
	
	public String serializeModel(){
		String serialization = null;
		this.m.enterCriticalSection(Lock.READ);
		try {
			serialization = serializeModel(this.m);
		} finally {
			this.m.leaveCriticalSection();
		}
		return serialization;
	}
	
	public static String serializeModel(Model m){
		StringWriter sw = new StringWriter();
		m.write(sw, "TTL");
		return sw.toString();
	}
	
	/**
	 * 
	 * @param parent if true, the annouce is for a parent, else it is for a child
	 * @return
	 */
	public synchronized String getAnnouncement(Boolean parent){
		Model m = this.getAnnounceModel(parent);
		Map<String, String> subs = new HashMap<String, String>();
		subs.put("id", Node.getInstance().getName());
		StringWriter sw = new StringWriter();
		m.write(sw, "TTL");
		return sw.toString();
	}
	
	/**
	 * 
	 * @param parent if true, the annouce is for a parent, else it is for a child
	 * @return
	 */
	public synchronized Model getAnnounceModel(Boolean parent){
		Map<String, String> subs = new HashMap<String, String>();
		subs.put("id", Node.getInstance().getName());
		try {
			if(parent) {
				return this.constructModel(
					this.queries.getTemplateQueries()
						.get("get_parent_description").substitute(subs));
			} else {
				return this.constructModel(
						this.queries.getTemplateQueries()
							.get("get_children_description").substitute(subs));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<String> getObservedProperties(String sensorURI){
		List<String> properties = new ArrayList<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("sensor", sensorURI);
		try {
			String query = this.queries.getTemplateQueries().get("get_properties").substitute(substitution);
			for(Map<String, String> result : this.queryModel(query)){
				properties.add(result.get("property"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return properties;
	}
	
	public List<String> getFeaturesOfInterest(String sensorURI){
		List<String> features = new ArrayList<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("sensor", sensorURI);
		try {
			String query = this.queries.getTemplateQueries().get("get_features").substitute(substitution);
			for(Map<String, String> result : this.queryModel(query)){
				features.add(result.get("feature"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return features;
	}
	
	public Resource validateModel(){
		Resource r = null;
		this.m.enterCriticalSection(Lock.READ);
		try {
			r = ValidationUtil.validateModel(this.m, this.m, true);
		} finally {
			this.m.leaveCriticalSection();
		}
		return r;
	}
	
	public Model executeRules(Boolean activateNodeElements, Boolean deactivateNodeElements){
		Model inferences = null;
		this.m.enterCriticalSection(Lock.WRITE);
		try {
			if(activateNodeElements){
				Node.getInstance().getShapesManager().activateNodeSensitiveElements();
			}
			inferences = RuleUtil.executeRules(this.m, this.m, null, null);
			this.m.add(inferences);
			if(deactivateNodeElements){
				Node.getInstance().getShapesManager().deactivateNodeSensitiveElements();
			}
		} finally {
			this.m.leaveCriticalSection();
		}
		return inferences;
	}
}
