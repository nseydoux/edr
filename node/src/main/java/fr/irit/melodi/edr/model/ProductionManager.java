package fr.irit.melodi.edr.model;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.sparql.exceptions.IncompleteSubstitutionException;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;

public class ProductionManager {
	private static final Logger LOGGER = LogManager.getLogger(InterestManager.class);
	public static Set<Entry<String, String>> prefixes;
	private FolderManager queries;
	
	static{
		prefixes = new HashSet<Entry<String, String>>();
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("sh", "<http://www.w3.org/ns/shacl#>"));
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("edr", "<https://w3id.org/laas-iot/edr#>"));
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("ex", "<http://example.com/ns#>"));
	}
	
	public ProductionManager(){
		try {
			this.queries = new FolderManager("queries/productions");
			this.queries.loadQueries();
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}
	}
	
	public void initializeProduction(){
		for(Map<String, String> sensor : Node.getInstance().getConfiguration().getSensors()){
			E_Property p = E_Property.fromString(sensor.get("produces"));
			if(p != null){
				this.addProduction(p.getUri());
			} else {
				LOGGER.warn("Bad property name : "+sensor.get("produces"));
			}
		}
	}
	
	public void addRule(Rule r){
		for(String property : r.getHead()){
			LOGGER.debug(Node.getInstance().getName()+" now produces "+property);
			this.addProduction(property);
		}
	}
	
	public void transferRules(String node, Set<String> rules){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("node", node);
		for(String rule : rules){
			substitution.put("rule", rule);
			try {
				Node.getInstance().getKBManager().updateModel(this.queries.getTemplateQueries().get("transfer_rule").substitute(substitution));
			} catch (IncompleteSubstitutionException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param remote
	 * @return true if the productions were updated
	 */
	public boolean addDownstreamRemote(String remote){
		Set<String> productionsBefore = this.getProductions(Node.getInstance().getUri());
		Map<String, String> substitution = new HashMap<>();
		substitution.put("remote", remote);
		substitution.put("self", Node.getInstance().getUri());
		String addQuery;
		try {
			addQuery = this.queries.getTemplateQueries().get("add_downstream").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(addQuery);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		Set<String> productionsAfter = this.getProductions(Node.getInstance().getUri());
		LOGGER.debug("Productions before adding "+remote+" : "+productionsBefore);
		LOGGER.debug("Productions after adding "+remote+" : "+productionsAfter);
		return !productionsBefore.equals(productionsAfter);
	}
	
	public Set<String> getPartialProducers(String interestURI){
		Set<String> targets = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("interest", interestURI);
		String query=null;
		try {
			query = this.queries.getTemplateQueries().get("get_unadvertised_partial_producers").substitute(substitution);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		for(Map<String, String> row : Node.getInstance().getKBManager().queryModel(query)){
			targets.add(row.get("producer"));
		}
		return targets;
	}
	
	public Set<String> getProducers(String interestURI){
		Set<String> targets = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("interest", interestURI);
		String query=null;
		try {
			query = this.queries.getTemplateQueries().get("get_unadvertised_producers").substitute(substitution);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		for(Map<String, String> row : Node.getInstance().getKBManager().queryModel(query)){
			targets.add(row.get("producer"));
		}
		return targets;
	}
	
	public void addProduction(String propertyURI){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("property", propertyURI);
		substitution.put("node", Node.getInstance().getUri());
		String addQuery;
		try {
			addQuery = this.queries.getTemplateQueries().get("add_production").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(addQuery);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}	
	
	public Set<String> getProductions(String node){
		Set<String> productions = new HashSet<>();
		LOGGER.trace("Listing productions of "+node);
		Map<String, String> substitution = new HashMap<>();
		substitution.put("node", node);
		String query;
		try {
			query = this.queries.getTemplateQueries().get("get_production").substitute(substitution);
			
			for(Map<String, String> production : Node.getInstance().getKBManager().queryModel(query)){
				productions.add(production.get("property"));
			}
			LOGGER.trace(node+" produces "+productions.toString());
			return productions;
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Boolean hasNonReasnoningProducingChild(String property){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		substitution.put("property", property);
		String query;
		try {
			query = this.queries.getTemplateQueries().get("get_downstream_nonreasoning_producers").substitute(substitution);
			return Node.getInstance().getKBManager().askModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}
}
