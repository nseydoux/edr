package fr.irit.melodi.edr.model;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.sparql.exceptions.IncompleteSubstitutionException;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;
import fr.irit.tools.queries.QueryEngine;

public class TopologyManager {
	private static final Logger LOGGER = LogManager.getLogger(TopologyManager.class);
	public static Set<Entry<String, String>> prefixes;
	private FolderManager queries;
	
	
	static{
		prefixes = new HashSet<Entry<String, String>>();
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("sh", "<http://www.w3.org/ns/shacl#>"));
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("edr", "<https://w3id.org/laas-iot/edr#>"));
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("ex", "<http://example.com/ns#>"));
	}
	
	public TopologyManager(){
		try {
			this.queries = new FolderManager("queries/topology");
			this.queries.loadQueries();
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}
	}
	
	public void addDownstreamRemote(String remote){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("remote", remote);
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("add_downstream_remote").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public static String normalizeId(Integer id){
		if(id < 10)
			return "000"+id.toString();
		else if (id < 100)
			return "00"+id.toString();
		else if (id < 1000)
			return "0"+id.toString();
		else
			return id.toString();
	}
	
	public String extractURIFromSensorEndpoint(String endpoint){
		Integer id = Integer.parseInt(endpoint.split(":")[2]) - 9000;
		return Node.BASE_URI+"sensor"+normalizeId(id);
	}
	
	public void addDownstreamSensor(String remoteEndpoint, String propertyClass, String propertyInstance, String feature){
		LOGGER.info("Adding a downstream sensor");
		Map<String, String> substitution = new HashMap<>();
		substitution.put("remote", this.extractURIFromSensorEndpoint(remoteEndpoint));
		substitution.put("self", Node.getInstance().getUri());
		substitution.put("propertyInstance", propertyInstance);
		substitution.put("propertyClass", propertyClass);
		if(feature != null){
			substitution.put("feature", feature);
			LOGGER.debug("1 Initializing feature "+feature);
		} else if(Node.getInstance().getConfiguration().getFeature() != null){
			substitution.put("feature", Node.getInstance().getConfiguration().getFeature());
			LOGGER.debug("2 Initializing feature "+Node.getInstance().getConfiguration().getFeature());
		} else {
			substitution.put("feature", Node.getInstance().getUri()+"feature");
			LOGGER.debug("3 Initializing feature "+Node.getInstance().getUri()+"feature");
		}
		try {
			String query = this.queries.getTemplateQueries().get("add_downstream_sensor").substitute(substitution);
			LOGGER.debug(query);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public void addUpstreamRemote(String remote){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("remote", remote);
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("add_upstream_remote").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public String extractNodeFromAnnounce(Model announce){
		String query = this.queries.getQueries().get("get_node");
		List<Map<String, String>> results = QueryEngine.selectQuery(query, announce);
		if(results.size() != 0){
			return results.get(0).get("node");
		} else {
			return null;
		}
	}
	
	public List<String> getConnectedNodes(){
		List<String> nodes = new ArrayList<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("get_connected_nodes").substitute(substitution);
			for(Map<String, String> result :Node.getInstance().getKBManager().queryModel(query)){
				nodes.add(result.get("node"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return nodes;
	}
	
	public Set<String> getDownstreamNodes(){
		Set<String> nodes = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("get_downstream_nodes").substitute(substitution);
			
			for(Map<String, String> result : Node.getInstance().getKBManager().queryModel(query)){
				nodes.add(result.get("node"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return nodes;
	}
	
	public Set<String> getReasoningDownstreamNodes(){
		Set<String> nodes = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("get_reasoning_downstream_nodes").substitute(substitution);
			for(Map<String, String> result : Node.getInstance().getKBManager().queryModel(query)){
				nodes.add(result.get("node"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return nodes;
	}
	
	public Set<String> getUpstreamNodes(){
		Set<String> nodes = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("get_upstream_nodes").substitute(substitution);
			for(Map<String, String> result : Node.getInstance().getKBManager().queryModel(query)){
				nodes.add(result.get("node"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return nodes;
	}
	
	public List<Map<String, String>> getConnectedNodesEndpoints(){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("get_nodes_endpoints").substitute(substitution);
			List<Map<String, String>> results = Node.getInstance().getKBManager().queryModel(query);
			for(Map<String, String > result : results){
				if(result.get("endpoint").contains("^^")){
					LOGGER.debug("The endpoint has an xsd type");
					// Removal of the xsd type if any
					result.put("endpoint", result.get("endpoint").split("\\^\\^")[0]);
				}
			}
			return results;
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getEndpoint(String node){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("node", node);
		String endpoint="";
		try {
			String query = this.queries.getTemplateQueries().get("get_node_endpoint").substitute(substitution);
			List<Map<String, String>> results = Node.getInstance().getKBManager().queryModel(query);
			for(Map<String, String > result : results){
				if(result.get("endpoint").contains("^^")){
					LOGGER.debug("The endpoint has an xsd type");
					// Removal of the xsd type if any
					endpoint = result.get("endpoint").split("\\^\\^")[0];
				} else {
					endpoint = result.get("endpoint");
				}
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return endpoint;
	}
	
	public Boolean hasSensorForProperty(String property){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("node", Node.getInstance().getUri());
		substitution.put("property", property);
		try {
			String query = this.queries.getTemplateQueries().get("has_sensor_for_property").substitute(substitution);
			return Node.getInstance().getKBManager().askModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return null;
	}
}
