package fr.irit.melodi.edr.node;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.topbraid.shacl.util.ModelPrinter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.irit.melodi.edr.configuration.E_Centralization;
import fr.irit.melodi.edr.configuration.NodeConfiguration;
import fr.irit.melodi.edr.errors.UnimplementedFeatureException;
import fr.irit.melodi.edr.in.server.RESTServer;
import fr.irit.melodi.edr.model.DataEnricher;
import fr.irit.melodi.edr.model.DeductionManager;
import fr.irit.melodi.edr.model.E_Property;
import fr.irit.melodi.edr.model.InterestManager;
import fr.irit.melodi.edr.model.KBManager;
import fr.irit.melodi.edr.model.ProductionManager;
import fr.irit.melodi.edr.model.RuleManager;
import fr.irit.melodi.edr.model.ShapesManager;
import fr.irit.melodi.edr.model.TopologyManager;
import fr.irit.melodi.edr.out.resources.AnnouncerClient;
import fr.irit.melodi.edr.out.resources.DataClient;
import fr.irit.melodi.edr.out.resources.SubscriptionClient;
import fr.irit.melodi.sparql.exceptions.IncompleteSubstitutionException;

public class Node {
	private static final Logger LOGGER = LogManager.getLogger(Node.class);
	private static final Logger LOGFILE = LogManager.getLogger("LogFile");
	
	public static final String BASE_URI="http://example.com/ns#";
	
	private static Node instance;
	private KBManager kb;
	private RuleManager rm;
	private InterestManager im;
	private ProductionManager pm;
	private TopologyManager tm;
	private ShapesManager sm;
	private DeductionManager dm;
	private RESTServer server;
	private NodeConfiguration config;
	private boolean configured = false;
	private String uri;
	private DataEnricher enricher;
	private Boolean announced;
	
	static {
    	// Necessary for jena and fuseki configuration
    	PropertyConfigurator.configure(System.getProperty("log4j.configuration"));
    }
	
	public static Node getInstance(){
		if(Node.instance == null){
			Node.instance = new Node();
		}
		return Node.instance;
	}
	
	private Node(){
		this.kb = new KBManager();
		this.rm = new RuleManager();
		this.im = new InterestManager();
		this.tm = new TopologyManager();
		this.pm = new ProductionManager();
		this.sm = new ShapesManager();
		this.dm = new DeductionManager();
		this.configured = false;
		this.announced = false;
	}
	
	private String buildURI(){
		if(this.configured){
			return BASE_URI+this.config.getName();
		} else {
			return BASE_URI+(new Random()).nextInt(10000);
		}
	}
	
	public String getName(){
		return this.config.getName();
	}
	
	public NodeConfiguration getConfiguration(){
		return this.config;
	}
	
	public synchronized Boolean isAnnounced(){
		return this.announced;
	}
	
	public synchronized void setAnnounced(Boolean annouced){
		this.announced = annouced;
	}
	
	public void initializeSelfRepresentation(){
		Map<String, String> substitution = new HashMap<String, String>();
		substitution.put("self_uri", this.uri);
		substitution.put("service_uri", this.uri+"Service");
		substitution.put("id", this.config.getName());
		substitution.put("endpoint", this.config.getHost()+":"+this.config.getPort());
		substitution.put("reasoning", this.config.getReasoning().toString());
		try {
			LOGGER.debug(this.kb.getFolderManager().getTemplateQueries().toString());
			this.getKBManager().updateModel(this.kb.getFolderManager().getTemplateQueries().get("self_node").substitute(substitution));
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public void setConfiguration(String path){
		LOGGER.debug("Reading node config");
		ObjectMapper mapper = new ObjectMapper();
		try {
			this.config = mapper.readValue(new File(path), NodeConfiguration.class);
			this.configured = true;
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		LOGGER.debug("Building node components");
		this.enricher = new DataEnricher();
		this.kb.setQueryFolder(this.config.getQueries());
		if(!this.config.getLocal()){
			for(String tboxVoc : this.config.getTbox()){
				this.kb.loadModel(tboxVoc);
			}
		}
		this.uri = this.buildURI();
		// Initialization of the node's self representation
		this.initializeSelfRepresentation();
		this.pm.initializeProduction();
		LOGGER.debug("Starting node REST server");
		this.server = new RESTServer(this.config.getHost(), this.config.getName(), this.config.getPort());
		this.server.run();
		this.setConfigured(true);
		LOGGER.debug("Node "+this.getName()+" configured");
	}
	
	private synchronized boolean getConfigured(){
		return this.configured;
	}
	
	private synchronized void setConfigured(boolean c){
		this.configured = c;
	}
	
	public KBManager getKBManager(){
		return this.kb;
	}
	
	public RuleManager getRuleManager(){
		return this.rm;
	}
	
	public InterestManager getInterestManager(){
		return this.im;
	}
	
	public ProductionManager getProductionManager(){
		return this.pm;
	}
	
	public TopologyManager getTopologyManager(){
		return this.tm;
	}
	
	public ShapesManager getShapesManager(){
		return this.sm;
	}
	
	public DeductionManager getDeductionManager(){
		return this.dm;
	}
	
	public DataEnricher getDataEnricher(){
		return this.enricher;
	}
	
	public String getUri(){
		return this.uri;
	}
	
	public void addUpstreamNode(String remoteDescription){
		Model remoteModel = ModelFactory.createDefaultModel();
		StringReader sr = new StringReader(remoteDescription);
		remoteModel.read(sr, Node.BASE_URI, "TTL");
		this.enricher.addSensorData(remoteModel);
		String remoteURI = this.tm.extractNodeFromAnnounce(remoteModel);
		this.tm.addUpstreamRemote(remoteURI);	
		// Any relevant information regarding remote node production are included in the remoteDescription
		this.rm.updateAllDownStreamNodes();
	}
	
	public synchronized void addDownstreamNode(String remoteDescription) throws UnimplementedFeatureException{
		Model remoteModel = ModelFactory.createDefaultModel();
		StringReader sr = new StringReader(remoteDescription);
		remoteModel.read(sr, Node.BASE_URI, "TTL");
		this.getKBManager().addToModel(remoteModel);
		String remoteURI = this.tm.extractNodeFromAnnounce(remoteModel);
		this.tm.addDownstreamRemote(remoteURI);
		Node.getInstance().getShapesManager().activateNodeSensitiveElements();
		// XXX Used to be true/false 
		Node.getInstance().getRuleManager().triggerReasoning(true, true);
		if(E_Centralization.rulePropagationAllowed(Node.getInstance().getConfiguration().getCentralization())){
			this.rm.updateDownStreamNode(remoteURI);
		}
		if(this.pm.addDownstreamRemote(remoteURI)){
			this.announce();
		}
	}
	
	public void announce(){
		String announcement = this.kb.getAnnouncement(true);
		LOGGER.debug("Announcement : "+announcement);
		if(announcement != null){
			LOGGER.debug("Announcing to "+this.config.getRemotes().size()+" remotes");
			for(String remote : this.config.getRemotes()){
				LOGGER.debug("Announcing "+this.getName()+" to "+remote);
				String remoteDesc = AnnouncerClient.annouceToUpstream(remote, announcement);
				this.addUpstreamNode(remoteDesc);
			}
		} else {
			LOGGER.fatal("Annoucement generation failed for node "+this.getName());
		}
		LOGGER.debug("Node "+this.getName()+" announced.");
		this.setAnnounced(true);
	}
	
	public void subscribe() throws UnimplementedFeatureException{
		for(Map<String, String> sensor : this.config.getSensors()){
			String propertyInstance = Node.BASE_URI+Node.getInstance().getName()+sensor.get("produces");
			LOGGER.debug(sensor.toString());
			String feature = null;
			if(sensor.containsKey("feature")){
				feature = sensor.get("feature");
				propertyInstance = Node.BASE_URI+feature.split("#")[1]+sensor.get("produces");
			}
			this.getTopologyManager().addDownstreamSensor(
					sensor.get("endpoint"), 
					E_Property.fromString(sensor.get("produces")).getUri(), 
					propertyInstance,
					feature);
			LOGGER.trace("Registering to remote sensor at address "+sensor.get("endpoint"));
//			Node.getInstance().getShapesManager().activateNodeSensitiveElements();
			Node.getInstance().getRuleManager().triggerReasoning(true, true);
//			Node.getInstance().getShapesManager().deactivateNodeSensitiveElements();
		}
		// The subscription is done in two times in order to minimize the delay between subscriptions
		for(Map<String, String> sensor : this.config.getSensors()){
			SubscriptionClient.subscribeToSensor(sensor.get("endpoint"), this.server.getURL()+"/data/raw");
		}
	}
	
	public void processNewRawData(String data) throws UnimplementedFeatureException{
		// Data is dropped while the node isn't ready
		if(this.getConfigured()){
			Model enrichedData = this.enricher.enrichData(data);
			this.processNewData(enrichedData);
		}
	}
	
	public void processNewData(Model data) throws UnimplementedFeatureException{
		logModel(data);
		if(this.getDataEnricher().isObservation(data)){
//			Node.logTrace("Received observation,"+this.enricher.getObservation(data)+",");
			Tracer.observationReceived(this.enricher.getObservation(data));
			LOGGER.debug("Received observation from lower node");
			this.enricher.addSensorData(data);
			Boolean isSensorKnown = false;
			// Case where the data has been produced by a sensor : the enrichment 
			// process is so that the property is not directly explicit
			this.reasonOverNewData(data);
			for(String rule : this.getRuleManager().getActiveRules()){
				this.getRuleManager().markDataProcessedByRule(data, rule);
			}
			for(String sensor : this.enricher.getFeaturedSensors(data)){
				LOGGER.trace("Sensor extracted from the enriched data : "+sensor);
				// It is assumed that sensors only produce observation regarding one property
				for(String property : this.pm.getProductions(sensor)){
					isSensorKnown = true;
					this.propagateNewData(data, property);
				}
			}
			if(!isSensorKnown){
				for(String property : this.getDataEnricher().getFeaturedPropertyClasses(data)){
					LOGGER.debug("Property found in observation : "+property);
					this.propagateNewData(data, property);
				}
			}
		} else if(data.size() > 0) {
			LOGGER.debug("Received deduction from lower node");
			LOGGER.debug(KBManager.serializeModel(data));
			this.enricher.addDeductionData(data);
			// Case where the new data is a deduction from another node
			for(Map<String, String> deductionInfo : this.dm.getDeductionType(data)){
				LOGGER.trace("Property appearing in the received data : "+deductionInfo.get("type"));
				this.propagateNewData(data, deductionInfo.get("type"));
				this.propagateDeductions(data, deductionInfo.get("rule"));
				this.reasonOverNewData(data);
			}
		} else {
			LOGGER.warn("Received empty message");
		}
	}
	
	public void propagateDeductions(Model data, String rule){
		LOGGER.debug("Propagating result from "+rule+" to interested nodes...");
		String serializedEnrichedData ="";
		StringWriter sw = new StringWriter();
		data.write(sw, "TTL");
		serializedEnrichedData = sw.toString();
		for(String node : this.im.getRuleResultConsumers(rule)){
			LOGGER.debug("Node "+node+" is interested in the results of "+rule);
			String endpoint = this.tm.getEndpoint(node);
			String obs = this.enricher.getObservation(data);
			if(obs != null){
				Tracer.observationSent(this.enricher.getObservation(data), node);
			}
			DataClient.sendData(endpoint, serializedEnrichedData);
		}
	}
	
//	public void propagateDeductions(Model inference){
//		LOGGER.debug("Propagating deduced results to interested nodes...");
//		for(Map.Entry<String, Set<String>> remoteInterests : this.im.listRemoteInterestsByType().entrySet()){
//			for(Map<String, String> deducedType : this.dm.getDeductionType(inference)){
//				if(remoteInterests.getKey().equals(deducedType.get("type"))){
//					for(String node : remoteInterests.getValue()){
//						LOGGER.debug("Node "+node+" is interested in "+deducedType.get("type"));
//						String endpoint = this.tm.getEndpoint(node);
//						StringWriter sw = new StringWriter();
//						Model deduction = this.dm.describeDeduction(deducedType.get("deduction"), inference);
//						deduction.write(sw, "TTL");
//						Tracer.observationSent(deducedType.get("deduction"), node);
//						DataClient.sendData(endpoint, sw.toString());
//					}
//				}
//			}
//		}
//	}
	
	public void propagateDeductions(Model inference){
		LOGGER.debug("Propagating deduced results to interested nodes...");
		for(Map<String, String> row : this.dm.listDeductions(inference)){
			for(String node : this.im.getRuleResultConsumers(row.get("rule"))){
				LOGGER.debug("Node "+node+" is interested in the results of "+row.get("rule"));
				String endpoint = this.tm.getEndpoint(node);
				StringWriter sw = new StringWriter();
				Model deduction = this.dm.describeDeduction(row.get("deduction"), inference);
				deduction.write(sw, "TTL");
				Tracer.observationSent(row.get("deduction"), node);
				DataClient.sendData(endpoint, sw.toString());
			}
		}
	}
	
	public void propagateNewData(Model data, String property){
		this.enricher.addPropertyToData(data, property);
		LOGGER.debug("Propagating new data about "+property+" to interested nodes...");
		Set<String> interestedNodes = this.im.getInterestedNodes(property);
		String serializedEnrichedData ="";
		if(interestedNodes.size() > 0){
			StringWriter sw = new StringWriter();
			data.write(sw, "TTL");
			serializedEnrichedData = sw.toString();
		}
		for(String interested : interestedNodes){
			LOGGER.debug("Node "+interested+" is interested in "+property);
			String endpoint = this.tm.getEndpoint(interested);
			String obs = this.enricher.getObservation(data);
			if(obs != null){
				Tracer.observationSent(this.enricher.getObservation(data), interested);
			}
			DataClient.sendData(endpoint, serializedEnrichedData);
		}
	}
	
	public void sendData(Model data, String targetUri, String endpoint){
		LOGGER.debug("Sending deduction to "+targetUri);
		String serializedEnrichedData ="";
		StringWriter sw = new StringWriter();
		data.write(sw, "TTL");
		serializedEnrichedData = sw.toString();
		Tracer.observationSent(this.enricher.getObservation(data), targetUri);
		DataClient.sendData(endpoint, serializedEnrichedData);
	}
	
	public void reasonOverNewData(Model data) throws UnimplementedFeatureException{
		LOGGER.debug("Reasoning over new data...");
		Tracer.reasoningStarts(this.enricher.getObservation(data));
		Model inference = Node.getInstance().getRuleManager().triggerReasoning(false, false);
		if(inference != null){
			LOGGER.debug("Inferred new knowledge");
			Tracer.reasoningEnds(this.enricher.getObservation(data), true);
			this.propagateDeductions(inference);
			this.getDataEnricher().addDeductionData(inference);
		} else {
			Tracer.reasoningEnds(this.enricher.getObservation(data), false);
		}
		LOGGER.debug("Reasoning ends.");
	}
	
	public void processInference(Model inferences){
		if(Node.getInstance().getDeductionManager().containsDeductions(inferences)){
			LOGGER.debug("New deductions");
			this.propagateDeductions(inferences);
			this.getDataEnricher().addDeductionData(inferences);
		} else {
			LOGGER.debug("No new deductions");
		}
	}
	
	public void processUpstreamMessage(Model message) throws UnimplementedFeatureException{
		this.getKBManager().addToModel(message);
		this.im.propagateInterests();	
	}
	
	public static void logModel(Model m){
		StringWriter sw = new StringWriter();
		m.write(sw, "TTL");
		LOGFILE.info(sw.toString());
	}
	
	public static int main(String[] args) throws UnimplementedFeatureException {
		LOGGER.debug("Starting node");
		org.apache.logging.log4j.core.LoggerContext ctx =
		    (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
		ctx.reconfigure();
		if (args[0] == null) {
			LOGGER.fatal("Usage : mvn exec:java -Dconfig=<path to configuration>");
			return 1;
		}
		LOGGER.debug("Configuring node from "+args[0]);
		Node.getInstance().setConfiguration(args[0]);
		LOGGER.info("Publishing initial announce");
		Node.getInstance().announce();
		Node.getInstance().subscribe();
		LOGGER.info("Waiting for messages");
		return 0;
	}
}
