package fr.irit.melodi.edr.out.resources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;


public class DataClient {
	private static final Logger LOGGER = LogManager.getLogger(DataClient.class);
	
	public static void sendData(String endpoint, String data){
		Client client = ClientBuilder.newClient(new ClientConfig());
		LOGGER.debug("Sendind data to "+endpoint);
		Response r = client.target(endpoint)
	        .path("/data/enriched")
	        .request()
	        .post(Entity.entity(data, "text/turtle"));
		if(r.getStatus() == 200){
			LOGGER.debug("Data correclty transmitted");
		} else {
			LOGGER.error("Data transmission failed with status "+r.getStatusInfo());
		}
	}
}
