package fr.irit.melodi.edr.out.resources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;

public class SubscriptionClient {
	
	private static final Logger LOGGER = LogManager.getLogger(SubscriptionClient.class);
	
	public static void subscribeToSensor(String remoteUri, String listeningEndpoint){
		Client client = ClientBuilder.newClient(new ClientConfig());
		Boolean successfulConnection = false;
		while(!successfulConnection){
			try{
				Response r = client.target(remoteUri)
		        .path("/registration/listener")
		        .request()
		        .post(Entity.entity(listeningEndpoint, MediaType.TEXT_PLAIN));
				successfulConnection = true;
				if(r.getStatus() == 200){
					LOGGER.debug("Connected to sensor "+remoteUri);
				} else {
					LOGGER.error("Connection to sensor "+remoteUri+" failed : "+r.getStatusInfo());
				}
			} catch(Exception e){
				LOGGER.warn("Connection to sensor "+remoteUri+" impossible, retrying.");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}
