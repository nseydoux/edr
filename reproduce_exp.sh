#! /bin/bash

for i in {1..5}
do
	# echo "distribution 0 adp $i"
	# python3 initializer.py -t simulator/thesis/distribution_0_adp/localhost/ -o logs_distribution_0_adp_$i -a application-mockup/applications-coopis-local/ -d 310
	# echo "distribution 0 cdp $i"
	# python3 initializer.py -t simulator/thesis/distribution_0_cdp/localhost/ -o logs_distribution_0_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
	# echo "distribution 0 cip $i"
	# python3 initializer.py -t simulator/thesis/distribution_0_cip/localhost/ -o logs_distribution_0_cip_$i -a application-mockup/applications-coopis-local/ -d 310
	# echo "distribution 0 cir $i"
	# python3 initializer.py -t simulator/thesis/distribution_0_cir/localhost/ -o logs_distribution_0_cir_$i -a application-mockup/applications-coopis-local/ -d 310
	# echo "distribution 0 cdr $i"
	# python3 initializer.py -t simulator/thesis/distribution_0_cdr/localhost/ -o logs_distribution_0_cdr_$i -a application-mockup/applications-coopis-local/ -d 310

	echo "distribution 1 adp $i"
	python3 initializer.py -t simulator/thesis/distribution_1_adp/localhost/ -o logs_distribution_1_adp_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 1 cdp $i"
	python3 initializer.py -t simulator/thesis/distribution_1_cdp/localhost/ -o logs_distribution_1_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 1 cip $i"
	python3 initializer.py -t simulator/thesis/distribution_1_cip/localhost/ -o logs_distribution_1_cip_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 1 cir $i"
	python3 initializer.py -t simulator/thesis/distribution_1_cir/localhost/ -o logs_distribution_1_cir_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 1 cdr $i"
	python3 initializer.py -t simulator/thesis/distribution_1_cdr/localhost/ -o logs_distribution_1_cdr_$i -a application-mockup/applications-coopis-local/ -d 310

	echo "distribution 2 adp $i"
	python3 initializer.py -t simulator/thesis/distribution_2_adp/localhost/ -o logs_distribution_2_adp_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 2 cdp $i"
	python3 initializer.py -t simulator/thesis/distribution_2_cdp/localhost/ -o logs_distribution_2_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 2 cip $i"
	python3 initializer.py -t simulator/thesis/distribution_2_cip/localhost/ -o logs_distribution_2_cip_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 2 cir $i"
	python3 initializer.py -t simulator/thesis/distribution_2_cir/localhost/ -o logs_distribution_2_cir_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 2 cdr $i"
	python3 initializer.py -t simulator/thesis/distribution_2_cdr/localhost/ -o logs_distribution_2_cdr_$i -a application-mockup/applications-coopis-local/ -d 310

	echo "distribution 3 adp $i"
	python3 initializer.py -t simulator/thesis/distribution_3_adp/localhost/ -o logs_distribution_3_adp_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 3 cdp $i"
	python3 initializer.py -t simulator/thesis/distribution_3_cdp/localhost/ -o logs_distribution_3_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 3 cip $i"
	python3 initializer.py -t simulator/thesis/distribution_3_cip/localhost/ -o logs_distribution_3_cip_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 3 cir $i"
	python3 initializer.py -t simulator/thesis/distribution_3_cir/localhost/ -o logs_distribution_3_cir_$i -a application-mockup/applications-coopis-local/ -d 310
	echo "distribution 3 cdr $i"
	python3 initializer.py -t simulator/thesis/distribution_3_cdr/localhost/ -o logs_distribution_3_cdr_$i -a application-mockup/applications-coopis-local/ -d 310

	# echo "distribution 4 adp $i"
	# python3 initializer.py -t simulator/thesis/distribution_4_adp/localhost/ -o logs_distribution_4_adp_$i -a application-mockup/applications-coopis-local/ -d 310
	# echo "distribution 4 cdp $i"
	# python3 initializer.py -t simulator/thesis/distribution_4_cdp/localhost/ -o logs_distribution_4_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
	# echo "distribution 4 cip $i"
	# python3 initializer.py -t simulator/thesis/distribution_4_cip/localhost/ -o logs_distribution_4_cip_$i -a application-mockup/applications-coopis-local/ -d 310
	# echo "distribution 4 cir $i"
	# python3 initializer.py -t simulator/thesis/distribution_4_cir/localhost/ -o logs_distribution_4_cir_$i -a application-mockup/applications-coopis-local/ -d 310
	# echo "distribution 4 cdr $i"
	# python3 initializer.py -t simulator/thesis/distribution_4_cdr/localhost/ -o logs_distribution_4_cdr_$i -a application-mockup/applications-coopis-local/ -d 310

	tar -zcvf BACKUP_logs_distribution_01234_$i.tar.gz logs_distribution_?_???_$i
	rm -r logs_distribution_?_???_$i
done

# for i in {1..8}
# do
# 	# echo "adp $i"
# 	# python3 initializer.py -t simulator/ieee/minimal_local_adp/localhost/ -a application-mockup/applications-shapes/ -o logs_minimal_adp_$i/ -d 930
# 	# echo "cir $i"
# 	# python3 initializer.py -t simulator/ieee/minimal_local_cir/localhost/ -a application-mockup/applications-shapes/ -o logs_minimal_cir_$i/ -d 930
# 	# echo "cdp $i"
#   # python3 initializer.py -t simulator/ieee/minimal_local_cdp/localhost/ -a application-mockup/applications-shapes/ -o logs_minimal_cdp_$i/ -d 930
# 	# echo "cdr $i"
#   # python3 initializer.py -t simulator/ieee/minimal_local_cdr/localhost/ -a application-mockup/applications-shapes/ -o logs_minimal_cdr_$i/ -d 930
# 	# echo "cip $i"
#   # python3 initializer.py -t simulator/ieee/minimal_local_cip/localhost/ -a application-mockup/applications-shapes/ -o logs_minimal_cip_$i/ -d 930
# 	# echo "clone f0 adp $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_0_adp/localhost/ -o logs_clone_f_0_adp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f0 cir $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_0_cir/localhost/ -o logs_clone_f_0_cir_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f0 cdp $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_0_cdp/localhost/ -o logs_clone_f_0_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f0 cdr $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_0_cdr/localhost/ -o logs_clone_f_0_cdr_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f0 cip $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_0_cip/localhost/ -o logs_clone_f_0_cip_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f1 adp $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_1_adp/localhost/ -o logs_clone_f_1_adp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f1 cir $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_1_cir/localhost/ -o logs_clone_f_1_cir_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f1 cdp $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_1_cdp/localhost/ -o logs_clone_f_1_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f1 cdr $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_1_cdr/localhost/ -o logs_clone_f_1_cdr_$i -a application-mockup/applications-coopis-local/ -d 310
# 	# echo "clone f1 cip $i"
# 	# python3 initializer.py -t simulator/coopis/scala_syndream/clone_f_1_cip/localhost/ -o logs_clone_f_1_cip_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "clone f2 adp $i"
# 	python3 initializer.py -t simulator/coopis/clone_f_2_adp/localhost/ -o logs_clone_f_2_adp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "clone f2 cir $i"
# 	python3 initializer.py -t simulator/coopis/clone_f_2_cir/localhost/ -o logs_clone_f_2_cir_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "clone f2 cdp $i"
# 	python3 initializer.py -t simulator/coopis/clone_f_2_cdp/localhost/ -o logs_clone_f_2_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "clone f2 cdr $i"
# 	python3 initializer.py -t simulator/coopis/clone_f_2_cdr/localhost/ -o logs_clone_f_2_cdr_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "clone f2 cip $i"
# 	python3 initializer.py -t simulator/coopis/clone_f_2_cip/localhost/ -o logs_clone_f_2_cip_$i -a application-mockup/applications-coopis-local/ -d 310
# 	tar -zcvf BACKUP_logs_clone_f_2_$i.tar.gz logs_clone_f_?_???_$i
# 	rm -r logs_clone_f_?_???_$i
# done

# for i in {1..5}
# do
# 	echo "specialisation_baseline adp $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_baseline_adp/localhost/ -o logs_specialisation_baseline_adp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline cir $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_baseline_cir/localhost/ -o logs_specialisation_baseline_cir_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline cdp $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_baseline_cdp/localhost/ -o logs_specialisation_baseline_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline cdr $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_baseline_cdr/localhost/ -o logs_specialisation_baseline_cdr_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline cip $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_baseline_cip/localhost/ -o logs_specialisation_baseline_cip_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline adp $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_implementation_adp/localhost/ -o logs_specialisation_implementation_adp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline cir $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_implementation_cir/localhost/ -o logs_specialisation_implementation_cir_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline cdp $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_implementation_cdp/localhost/ -o logs_specialisation_implementation_cdp_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline cdr $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_implementation_cdr/localhost/ -o logs_specialisation_implementation_cdr_$i -a application-mockup/applications-coopis-local/ -d 310
# 	echo "specialisation_baseline cip $i"
# 	python3 initializer.py -t simulator/coopis/specialisation_implementation_cip/localhost/ -o logs_specialisation_implementation_cip_$i -a application-mockup/applications-coopis-local/ -d 310
# 	tar -zcvf BACKUP_logs_specialisation_$i.tar.gz logs_specialisation_*_???_$i
# 	rm -r logs_specialisation_*_???_$i
# done
