#! /bin/bash

logs_path=logs/thesis/iter_7
results_path=results/thesis/iter_7

for topo in {2..4}
do
	file_seed=logs_distrib_$topo
	result_seed=results_distrib_$topo
	# for iter in {1..1}
	# do
		echo "Computing results round $iter"
		results_folder=$results_path/$result_seed #"_$iter"
		mkdir -p $results_folder
		python3 results.py --adp $logs_path/$file_seed"_adp" --cir $logs_path/$file_seed"_cir"/ --cdp $logs_path/$file_seed"_cdp"/ --cdr $logs_path/$file_seed"_cdr"/ --cip $logs_path/$file_seed"_cip"/ -o $results_folder
		# python3 results.py --adp $logs_path/$file_seed"_adp_$iter" --cir $logs_path/$file_seed"_cir_$iter"/ --cdp $logs_path/$file_seed"_cdp_$iter"/ --cdr $logs_path/$file_seed"_cdr_$iter"/ --cip $logs_path/$file_seed"_cip_$iter"/ -o $results_folder
	# done
done
