import rdflib
from rdflib.namespace import Namespace
from rdflib.graph import Graph
import sys
import dateutil.parser
import datetime
import pytz
import glob
import json
import numpy as np
# The two following lines allow to compute results without an X server
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats
import matplotlib.mlab as mlab
from sklearn.neighbors import KernelDensity
import argparse
import pickle

###########################################################
###########################################################
## Raw data formatting functions
###########################################################
###########################################################

def prepare_trace(logs_folder_path):
    trace=[]
    for tracefile_path in glob.glob(logs_folder_path+"/*.trace"):
        with open(tracefile_path, "r") as tracefile:
            for line in tracefile:
                event = json.loads(line)
                trace.append(event)
    trace.sort(key=lambda x: x["time"])
    return trace

def prepare_graph(logs_folder_path):
    g = Graph()
    for file in glob.glob(logs_folder_path+"/*.ttl"):
        print("Parsing "+file)
        g.parse(file, format="ttl")
    return g

###########################################################
###########################################################
## Data refining functions
###########################################################
###########################################################

def compute_delay(graph):
    print("Querying graph...")
    qres = graph.query(
        """
        PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX san: <http://www.irit.fr/recherches/MELODI/ontologies/SAN#>
        SELECT ?ded ?obstimestamp ?dedtime ?rule ?node ?obs ?dedAppTimestamp
        WHERE {
            ?ded edr:deducedAt ?dedtime;
                edr:deducedWith ?rule;
                edr:deducedBy ?node;
                edr:receivedAt ?dedAppTimestamp;
                edr:deducedFrom  ?obs.
            ?obs a ssn:Observation ;
                edr:receivedAt ?obstimestamp.
         }
        """)
    print("Graph queried, analyzing results")
    rules = {}
    for row in qres.result:
        rule = row[3]
        node = row[4]
        obs_uri = row[5]
        if not rule in rules:
            rules[rule] = {}
        deduction_uri = row[0]
        deduction_time = dateutil.parser.parse(row[2])
        deduction_received = dateutil.parser.parse(row[6])
        last_symptom = dateutil.parser.parse(row[1])
        if not(deduction_uri in rules[rule]):
            rules[rule][deduction_uri] = {"uri":str(deduction_uri), "deduction_received":deduction_received, "deduction_time":deduction_time, "last_symptom":last_symptom, "node":node, "obs_uri":str(obs_uri)}
        else:
            if last_symptom > rules[rule][deduction_uri]["last_symptom"]:
                rules[rule][deduction_uri]["last_symptom"] = last_symptom
                rules[rule][deduction_uri]["obs_uri"] = str(obs_uri)
    print("Data collected, finalyzing delays")
    # Computation of the delays
    for rule in rules:
        for deduction in rules[rule]:
            delay = (rules[rule][deduction]["deduction_received"]-rules[rule][deduction]["last_symptom"]).total_seconds()
            rules[rule][deduction]["delay"]=delay
    return rules

# Returns (number of messages, average duration, std deviation)
def compute_hop_time(trace):
    trips = 0
    negative_trips = 0
    trip_times = []
    for record_origin in [x for x in trace if x["event"] == "Sending data"]:
        for record_destination in [y for y in trace if y["event"] == "Received observation"]:
            if record_origin["target"] == record_destination["node"] and record_origin["observation"] == record_destination["observation"]:
                trips += 1
                sent_time = dateutil.parser.parse(record_origin["time"])
                received_time = dateutil.parser.parse(record_destination["time"])
                trip_time = received_time - sent_time
                if(trip_time.total_seconds() > 0):
                    trip_times.append(trip_time.total_seconds())
                else:
                    negative_trips +=1
                break
    return trip_times

def compute_reasoning(trace):
    reasonings = {}
    for record_origin in [x for x in trace if x["event"] == "Reasoning starts" and "rule" in x]:
        for record_destination in [y for y in trace if y["event"] == "Reasoning ends" and "rule" in y]:
            if(record_origin["host"] == record_destination["host"]) and (record_origin["observation"] == record_destination["observation"]) and (record_origin["rule"] == record_destination["rule"]):
                begin_time = dateutil.parser.parse(record_origin["time"])
                end_time = dateutil.parser.parse(record_destination["time"])
                reasoning_time = end_time - begin_time
                # The actual reasoning event
                if(not record_origin["rule"] in reasonings):
                    reasonings[record_origin["rule"]] = []
                reasoning = {"time":reasoning_time.total_seconds(), "observation":record_origin["observation"], "host":record_origin["host"], "deduction":record_destination["deduction"], "moment":record_destination["time"]}
                if reasoning["time"]>0:
                    reasonings[record_origin["rule"]].append(reasoning)
                break
    return reasonings

# deduction is a boolean
def compute_reasoning_times(reasonings_trace, deduction):
    reasoning_times = {}
    for rule in reasonings_trace:
        for r in reasonings_trace[rule]:
            if r["deduction"] == str(deduction).lower() and r["time"] > 0:
                if not rule in reasoning_times:
                    reasoning_times[rule] = [r["time"]]
                else:
                    reasoning_times[rule].append(r["time"])
    return reasoning_times

def compute_avg_reasoning(reasoning_times):
    avg = {}
    dev = {}
    for rule in reasoning_times:
        avg[rule] = np.average([x for x in reasoning_times[rule]])
        dev[rule] = np.std([x for x in reasoning_times[rule]])
    return (avg, dev)

def compute_single_journey(deduction_uri, obs_uri, trace):
    journey = []
    for event in trace:
        if "observation" in event and (event["observation"] == obs_uri or event["observation"] == deduction_uri):
            journey.append(event)
    return journey

def display_journey(journey):
    for event in journey:
        print(event)

def summarize_journey(journey, deduction, rule):
    summary = {"idle":0, "transit":0, "processing":0, "additional-processing":0, "additional-idle":0}
    deduced = False
    for i in range(len(journey)):
        event = journey[i]
        if event["event"] == "Enrichment ended":
            summary["creation"] = event["time"]
            if i<len(journey)-1:
                summary["idle"] = (dateutil.parser.parse(journey[i+1]["time"]) - dateutil.parser.parse(event["time"])).total_seconds()
        # If the deducer sent the data, it is processed elsewhere
        if event["event"] == "Sending data":
            #if event["node"] != deduction["node"]:
            for j in range(len(journey[i:])):
                if journey[j]["event"] == "Received observation" and journey[j]["node"] == event["target"]:
                    summary["transit"] += (dateutil.parser.parse(journey[j]["time"]) - dateutil.parser.parse(event["time"])).total_seconds()
                    break
            else:
                # The application does not log the reception event
                naive_deduction_received = deduction["deduction_received"].replace(tzinfo=None)
                day_agnostic_deduction = dateutil.parser.parse(str(naive_deduction_received.time()))
                event_time = dateutil.parser.parse(event["time"])
                summary["transit"] += (day_agnostic_deduction - dateutil.parser.parse(event["time"])).total_seconds()
        if event["event"] == "Received observation" and i<len(journey)-1:
            summary["idle"] += (dateutil.parser.parse(journey[i+1]["time"]) - dateutil.parser.parse(event["time"])).total_seconds()
        if event["event"] == "Reasoning starts" and i<len(journey)-1:
            summary["processing"] += (dateutil.parser.parse(journey[i+1]["time"]) - dateutil.parser.parse(event["time"])).total_seconds()
            # if event["rule"] == rule:
            #     summary["deduction"] = journey[i+1]["time"]
            #     deduced = True
        if event["event"] == "Reasoning ends" and i<len(journey)-1:
            summary["idle"] += (dateutil.parser.parse(journey[i+1]["time"]) - dateutil.parser.parse(event["time"])).total_seconds()
        summary["delay"]=summary["idle"]+summary["transit"]+summary["processing"]
    if summary["transit"] <= 0:
        # print(journey)
        # print(summary)
        # print(deduction)
        return None
    return summary

def compute_journeys(graph, trace):
    print("To compute journeys, computing delays")
    rules = compute_delay(graph)
    journeys = []
    for rule in rules:
        for deduction in rules[rule]:
            # print("Searching for observation "+rules[rule][deduction]["obs_uri"])
            # print("delay is "+str(rules[rule][deduction]["delay"]))
            j = compute_single_journey(rules[rule][deduction]["uri"], rules[rule][deduction]["obs_uri"], trace)
            #display_journey(j)
            sj = summarize_journey(j, rules[rule][deduction], str(rule))
            if sj != None:
                journeys.append(sj)
    return journeys

def extract_delays_details(journeys):
    delays = []
    idles = []
    transits = []
    processings = []
    for j in journeys:
        delays.append(j["idle"]+j["transit"]+j["processing"])
    return delays

###########################################################
###########################################################
## Plotting functions
###########################################################
###########################################################

# Scatters the evolution of delays against time, to check whether there is a general tendancy
def scatter_delays(delays_list, folder):
    # for rule in delays:
    #     ded_delay = []
    #     ded_moment = []
    #     for deduction in delays[rule]:
    #         ded_delay.append(delays[rule][deduction]["delay"])
    #         ded_moment.append(delays[rule][deduction]["deduction_time"])
    #     plt.plot(ded_moment,ded_delay, "r+")
    #     plt.show()

    rules = set()
    for delays in delays_list:
        rules = rules.union(delays.keys())
    for rule in rules:
        index = 1
        fig_handle = plt.figure(1)
        plt_id = int(str(len(delays_list))+"11")
        ax0 = plt.subplot(plt_id)
        for delays in delays_list:
            if rule in delays:
                subplot_id = int(str(len(delays_list))+"1"+str(index))
                ax_n = plt.subplot(subplot_id, sharex=ax0, sharey=ax0)
                ded_delay = []
                ded_moment = []
                for deduction in delays[rule]:
                    if delays[rule][deduction]["delay"] > 0:
                        ded_delay.append(delays[rule][deduction]["delay"])
                        ded_moment.append(delays[rule][deduction]["deduction_time"])
                plt.plot(ded_moment,ded_delay, "r+")
            index += 1
        plt.xlabel('Time')
        plt.title(rule)
        plt.savefig(folder+"/delay_scatter_"+rule.split("#")[1], format='eps', dpi=600)
        plt.show()
        plt.close()

def get_bin_centers(bin_edges):
    centers = []
    for i in range(len(bin_edges)-1):
        centers.append((bin_edges[i]+bin_edges[i+1])/2.0)
    return centers

def plot_histogram_kde(name, ax, data, bins):
    #n, bins, patches = ax.hist(data, density=True, bins='doane')
    n, bins, patches = ax.hist(data, density=True, bins=bins)
    try:
        print("{0} values, from {1} to {2} - {3}".format(str(len(data)), str(min(data)), str(max(data)), name))
        if len(data) > 1:
            kernel = stats.gaussian_kde(data)
            # possible bandwidth method : silverman
            kernel.set_bandwidth(0.2)
            centers = get_bin_centers(bins)
            ax.plot(centers, kernel.evaluate(centers), '-', label="kernel = '{0}'".format("gaussian"))
    except np.linalg.linalg.LinAlgError:
        print(data)

def histogram_delays(delays_list, folder):
    rules = set()
    for delays in delays_list:
        rules = rules.union(delays.keys())
    for rule in rules:
        index = 1
        fig_handle = plt.figure(1)
        plt_id = int(str(len(delays_list))+"11")
        ax0 = plt.subplot(plt_id)
        for delays in delays_list:
            if rule in delays:
                subplot_id = int(str(len(delays_list))+"1"+str(index))
                ax_n = plt.subplot(subplot_id, sharex=ax0, sharey=ax0)
                ded_delay = []
                for deduction in delays[rule]:
                    if delays[rule][deduction]["delay"] > 0:
                        ded_delay.append(delays[rule][deduction]["delay"])
                plot_histogram_kde(ax_n, ded_delay, 75)
                with open(folder+"/ded_delays_"+str(delays_list.index(delays))+"_"+rule.split("#")[1], "wb") as f:
                    pickle.dump(ded_delay, f)
            index += 1
        plt.xlabel('Delays')
        plt.title(rule)
        plt.savefig(folder+"/delay_"+rule.split("#")[1],format='eps', dpi=300)
        with open(folder+"/"+rule.split("#")[1]+"_fig.pkl", "wb") as f:
            pickle.dump(fig_handle, f)
        plt.close()

def histogram_journeys(journeys_list, folder):
    index = 1
    fig_handle = plt.figure(1)
    plt_id = int(str(len(journeys_list))+"11")
    ax0 = plt.subplot(plt_id)
    for method, journeys in journeys_list.items():
        if len(journeys) > 0:
            subplot_id = int(str(len(journeys_list))+"1"+str(index))
            ax_n = plt.subplot(subplot_id, sharex=ax0, sharey=ax0)
            ded_delay = []
            plt.title(method)
            for journey in journeys:
                if "delay" in journey and journey["delay"] > 0 and journey["delay"]<7000:
                    ded_delay.append(journey["delay"])
            if len(ded_delay) == 0:
                print("For "+method+", a list of delays is empty")
                print(journeys)
            else:
                plot_histogram_kde(method, ax_n, ded_delay, 75)
            with open(folder+"/journ_delays_"+method, "wb") as f:
                pickle.dump(ded_delay, f)
            index += 1
    plt.xlabel('Delays')
    plt.savefig(folder+"/journ_delay",format='eps', dpi=300)
    with open(folder+"/"+"journey_fig.pkl", "wb") as f:
        pickle.dump(fig_handle, f)
    plt.close()

def stack_journeys(journeys_list, folder):
    centralized_transit = [j["transit"] for j in journeys_list[0]]
    centralized_idle = [j["idle"] for j in journeys_list[0]]
    centralized_processing = [j["processing"] for j in journeys_list[0]]
    width = 0.35       # the width of the bars: can also be len(x) sequence

    transit_mean = [np.mean(centralized_transit)]
    transit_std = [np.std(centralized_transit)]

    processing_mean = np.mean(centralized_processing)
    processing_std = np.std(centralized_processing)


    p1 = plt.bar([0], transit_mean, width, yerr=transit_std)
    p2 = plt.bar(0, processing_mean, width,
                 bottom=transit_mean, yerr=processing_std)

    # plt.ylabel('Scores')
    # plt.title('Scores by group and gender')
    # plt.xticks(0, ('G1'))
    # plt.yticks(np.arange(0, 81, 10))
    plt.legend((p1[0], p2[0]), ('Transit', 'Processing'))
    plt.show()


def scatter_reasonings(reasonings):
    for rule in reasonings:
        r_duration_deduction = []
        r_moment_deduction = []
        r_duration_nodeduction = []
        r_moment_nodeduction = []
        print(rule)
        for r in reasonings[rule]:
            if r["deduction"] == "true":
                r_duration_deduction.append(r["time"])
                r_moment_deduction.append(dateutil.parser.parse(r["moment"]))
            else:
                r_duration_nodeduction.append(r["time"])
                r_moment_nodeduction.append(dateutil.parser.parse(r["moment"]))
        plt.plot(r_moment_deduction,r_duration_deduction, "b+")
        plt.plot(r_moment_nodeduction,r_duration_nodeduction, "rx")
        plt.show()

# def histogram_reasonings(reasonings):
#     for rule in reasonings:
#         r_deduction_duration = []
#         r_nodeduction_duration = []
#         print(rule)
#         for r in reasonings[rule]:
#             if r["deduction"] == "true":
#                 r_deduction_duration.append(r["time"])
#             else:
#                 r_nodeduction_duration.append(r["time"])
#         # Two subplots, the axes array is 1-d
#         f, axarr = plt.subplots(2, sharex=True)
#         (mu_ded, sigma_ded) = norm.fit(r_deduction_duration)
#         n_ded, bins_ded, patches_ded = axarr[0].hist(r_deduction_duration,  normed=1,bins='auto', facecolor='blue')
#         y = mlab.normpdf(bins_ded, mu_ded, sigma_ded)
#         l = axarr[0].plot(bins_ded, y, 'k--', linewidth=2)
#         axarr[1].hist(r_nodeduction_duration,  bins=200, facecolor='red')
#         (mu_noded, sigma_noded) = norm.fit(r_nodeduction_duration)
#         n_noded, bins_noded, patches_noded = axarr[0].hist(r_deduction_duration, normed=1, bins='auto', facecolor='blue')
#         y = mlab.normpdf(bins_noded, mu_noded, sigma_noded)
#         l = axarr[1].plot(bins_noded, y, 'k--', linewidth=2)
#         plt.show()

def histogram_reasonings(reasonings_list, folder):
    rules = set()
    for reasonings in reasonings_list:
        rules = rules.union(reasonings.keys())
    for rule in rules:
        index = 1
        plt.figure(1)
        ax0 = plt.subplot(len(reasonings_list), 2, 1)
        for reasonings in reasonings_list:
            if rule in reasonings:
                r_deduction_duration = []
                r_nodeduction_duration = []
                for r in reasonings[rule]:
                    if r["deduction"] == "true":
                        r_deduction_duration.append(r["time"])
                    else:
                        r_nodeduction_duration.append(r["time"])
                if len(r_deduction_duration) > 1:
                    # Plotting reasoning leading to deductions
                    ax_n_ded = plt.subplot(len(reasonings_list), 2, index, sharex=ax0)
                    ax_n_ded.relim()
                    # update ax.viewLim using the new dataLim
                    ax_n_ded.autoscale_view()
                    plt.title("Index : "+str(index)+", deductions")
                    plot_histogram_kde(ax_n_ded, r_deduction_duration, 30)
                if len(r_nodeduction_duration) > 1:
                    # Plotting reasoning leading to nothing
                    ax_n_noded = plt.subplot(len(reasonings_list), 2, index+len(reasonings_list), sharex=ax0)
                    ax_n_noded.relim()
                    # update ax.viewLim using the new dataLim
                    ax_n_noded.autoscale_view()
                    plt.title("Index : "+str(index)+", no deductions")
                    plot_histogram_kde(ax_n_noded, r_nodeduction_duration, 20)
            index += 1
        plt.suptitle('Reasoning '+rule)
        plt.savefig(folder+"/reasoning_"+rule.split("#")[1], dpi=300)
        plt.close()

def count_messages(trace):
    counter = 0
    for event in trace:
        if event["event"] == "Sending data":
            counter+=1
    return counter

parser = argparse.ArgumentParser(description='Processes the traces of an edr execution.')
parser.add_argument('-c', '--centralized', help="the folder containing all logs files for the centralized approach", type=str)
parser.add_argument('-d', '--distributed', help="the folder containing all logs files for the distributed approach", type=str)
parser.add_argument('--adp', type=str)
parser.add_argument('--cir', type=str)
parser.add_argument('--cdp', type=str)
parser.add_argument('--cdr', type=str)
parser.add_argument('--cip', type=str)
parser.add_argument('-o', '--output', help="the folder to which results are sent", type=str)
parser.add_argument('--delay', help="computes delay times", action="store_true")
parser.add_argument('--scatter', help="compute a scatter plot instead of histograms", action="store_true")
parser.add_argument('--reasoning', help="computes reasoning times", action="store_true")
parser.add_argument('--hop', help="computes hop times", action="store_true")
parser.add_argument('--journey', help="computes the different elements of the delay (hop time, reasoning time, wait time)", action="store_true")

args = parser.parse_args()
if args.output == None:
    exit("Defining an output is mandatory")

# Raw data
traces = []
graphs = []

if args.centralized != None:
    print("Compiling centralized data from "+args.centralized)
    # if args.reasoning or args.hop or args.journey or args.delay:
    print("Appending centralized trace...")
    traces.append(prepare_trace(args.centralized))
    if args.delay or args.journey:
        print("Appending centralized graph...")
        graphs.append(prepare_graph(args.centralized))

if args.distributed  != None:
    print("Compiling distributed data from "+args.distributed)
    # if args.reasoning or args.hop or args.journey:
    print("Appending distributed trace...")
    traces.append(prepare_trace(args.distributed))
    if args.delay or args.journey:
        print("Appending distributed graph...")
        graphs.append(prepare_graph(args.distributed))

# Refined data
if args.delay and (args.centralized != None or args.distributed  != None):
    print("Computing delays...")
    journeys = []
    delays = []
    for graph in graphs:
        j = compute_journeys(graph, traces[graphs.index(graph)])
        journeys.append(j)
        delays.append(extract_delays_details(j))
        # delays.append(compute_delay(graph))
    with open(args.output+"/journeys_c.pkl", "wb") as f:
        pickle.dump(journeys[0], f)
    if len(journeys) > 1:
        with open(args.output+"/journeys_d.pkl", "wb") as f:
            pickle.dump(journeys[1], f)
    if args.scatter:
        scatter_delays(delays, args.output)
    else:
        #histogram_delays(delays, args.output)
        if len(journeys) > 0:
             histogram_journeys(journeys, args.output)
        pass
    with open(args.output+"/delays_c.pkl", "wb") as f:
        pickle.dump(delays[0], f)
    if len(delays) > 1:
        with open(args.output+"/delays_d.pkl", "wb") as f:
            pickle.dump(delays[1], f)
    del(graphs)

if args.adp != None or args.cir != None or args.cdp != None or args.cip != None or args.cdr != None:
    journeys = {}
    for method, folder in {"adp":args.adp,
            "cdp":args.cdp,
            "cip":args.cip,
            "cdr":args.cdr,
            "cir":args.cir}.items():
        if folder != None:
            print("Collecting results for "+method)
            trace = prepare_trace(folder)
            graph = prepare_graph(folder)
            journey = compute_journeys(graph, trace)
            with open(args.output+"/journeys_{0}.pkl".format(method), "wb") as f:
                pickle.dump(journey, f)
            journeys[method]=journey
            delay = extract_delays_details(journey)
            with open(args.output+"/delays_{0}.pkl".format(method), "wb") as f:
                pickle.dump(delay, f)
            with open(args.output+"/messages_{0}.txt".format(method), "w") as f:
                f.write(str(count_messages(trace)))
            del(trace)
            del(graph)
            del(delay)
    if len(journeys) > 0:
         histogram_journeys(journeys, args.output)


if args.hop:
    print("Computing hop times...")
    hop_times = []
    for trace in traces:
        hop_times.append(compute_hop_time(trace))

if args.reasoning:
    print("Computing reasoning times")
    reasonings = []
    for trace in traces:
        reasonings.append(compute_reasoning(trace))
    histogram_reasonings(reasonings, args.output)

if args.journey:
    print("Computing journey...")
    journeys = []
    compute_journeys(graphs[0], traces[0])





#
# g = Graph()
# for file in glob.glob(logs_folder_path+"/*.ttl"):
#     g.parse(file, format="ttl")
# delays = compute_delay(g)
# # scatter_delays(delays)
# histogram_delays(delays)
#
# reasonings = compute_reasoning(trace)

# maintenant il faut superposer les graphiques centralisé et distribué pour pouvoir les comparer

# scatter_reasonings(reasonings)
# histogram_reasonings(reasonings)

# reasoning_times_deduction = compute_reasoning_times(reasonings, True)
# reasoning_times_no_deduction = compute_reasoning_times(reasonings, False)
# avg_deduction, std_deduction = compute_avg_reasoning(reasoning_times_deduction)
# avg_no, std_no = compute_avg_reasoning(reasoning_times_no_deduction)
# rules = []
# times = []
# for rule in avg_deduction:
#     rules.append(rule)
#     times.append(avg_deduction[rule])
#
# with open(result_path, "w") as result_file:
#     result_file.write("{0},{1},{2},{3},{4},\n".format("Rule", "Processing time (deduction)", "Deviation (deduction)", "Processing time (no deduction)", "Deviation (no deduction)"))
#     for rule in avg_deduction:
#         result_file.write("{0},{1},{2},{3},{4},\n".format(rule, avg_deduction[rule], std_deduction[rule], avg_no[rule], std_no[rule]))
#     result_file.write(",,,,\n")
#     result_file.write("Hops, Hop time (avg), Hop time (deviation)\n")
#     hop_time = compute_hop_time(trace, g)
#     result_file.write("{0}, {1}, {2}\n".format(hop_time[0], hop_time[1], hop_time[2]))
    # result_file.write("Delay (avg), Delay (deviation)\n")
    # result_file.write("{0}, {1}\n".format(delay[0], delay[1]))

# y_pos = np.arange(len(rules))
# plt.bar(y_pos, times, align='center', alpha=0.5)
# plt.xticks(y_pos, rules)
# plt.ylabel('Avg time')
#
# plt.show()
