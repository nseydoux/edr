package fr.irit.melodi.sensor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.sensor.configuration.Configuration;

public class Sensor extends Thread {	
	private static final Logger LOGGER = LogManager.getLogger(Sensor.class);
	public static final String observationHeader = "chrono,name,value,quality,comment\n";
	private Configuration config;
	private Boolean replay;
	private String dataFile;
	private BufferedReader replayReader;
	
	public Sensor(Configuration config) throws FileNotFoundException{
		this.config = config;
		if(config.getDatafile() != null){
			this.replay = true;
			this.dataFile = config.getDatafile();
			this.replayReader = new BufferedReader(new FileReader(dataFile));
		} else {
			this.replay = false;
			this.dataFile = null;
		}
	}
	
	public Float buildNewData(){
		Random r = new Random();
		if(this.config.getMeasureType().equals("float")){
			Float min = this.config.getLowerBound();
			Float max = this.config.getUpperBound();
			return min + r.nextFloat()*(max-min);
		} else if(this.config.getMeasureType().equals("boolean")){
			if(r.nextFloat() < this.config.getRate()){
				return 1.0f;
			} else {
				return 0.0f;
			}
		} else {
			return 0.0f;
		}
	}
	
	public String buildObservation(String timestamp, String id, Float data, String description){
		return timestamp+","+id+","+data+",192,"+description+"\n";
	}
	
	public String buildTimestamp(){
		Date now = Calendar.getInstance().getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SX");
		return formatter.format(now).replaceAll(" ", "T");
	}
	
	public String replaceSensorName(String row){
		String[] csv = row.split(",");
		return csv[0]+","+this.config.getName()+","+csv[2]+","+csv[3]+",\n";
	}
	
	public boolean isObservationValid(String row){
		String[] csv = row.split(",");
		return !csv[2].contains("nan");
	}

	public void run(){
		try {
			String observation="";
			Thread.sleep(this.config.getInitialSleep());
			while(true){
				// This boolean is updated when replaying values that might be nan 
				Boolean observationValid = true;
				Thread.sleep(this.config.getCycleDuration());
				if(!this.replay){
					observation = 
						observationHeader+
						this.buildObservation(
								buildTimestamp(),
								this.config.getName(),
								this.buildNewData(),
								"");
				} else {
					try {
						observation = this.replayReader.readLine();
						// Case where the end of the feed has been reached
//						if(observation == null){
//							this.replayReader = new BufferedReader(new FileReader(dataFile));
//							observation = this.replayReader.readLine();
//						}
						observation+="\n";
						observation = this.replaceSensorName(observation); 
						observationValid = this.isObservationValid(observation);
						observation = observationHeader+observation;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(observationValid){
					LOGGER.debug(observation);
					Controller.getInstance().newObservationProduced(this.config.getPort(), observation);
				} else {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
