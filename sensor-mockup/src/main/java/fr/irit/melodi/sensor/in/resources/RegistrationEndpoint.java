package fr.irit.melodi.sensor.in.resources;

import java.net.HttpURLConnection;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.Request;

import fr.irit.melodi.sensor.model.Model;

@Path("/registration")
public class RegistrationEndpoint {

	@GET
	@Path("/ping")
	public Response ping(@Context Request httpRequest){
		return Response.status(200).entity("pong "+httpRequest.getLocalPort()).build();
	}
	
	@POST
	@Path("/listener")
	public Response register(String listeningEndpoint, @Context Request httpRequest){
		Model.getInstance(httpRequest.getLocalPort()).addListener(listeningEndpoint);
		return Response.status(HttpURLConnection.HTTP_OK)
        .entity("registered").build();
	}
	
	@GET
	@Path("/listeners")
	public Response listListeners(@Context Request httpRequest){
		String listeners = "[";
		for(String l : Model.getInstance(httpRequest.getLocalPort()).getListeners()){
			listeners += l+",";
		}
		listeners+="]";
		return Response.status(HttpURLConnection.HTTP_OK)
		        .entity(listeners).build();
	}
}
