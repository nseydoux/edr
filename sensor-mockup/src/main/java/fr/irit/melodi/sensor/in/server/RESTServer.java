package fr.irit.melodi.sensor.in.server;

import java.net.URI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import fr.irit.melodi.sensor.in.resources.RegistrationEndpoint;

public class RESTServer extends Thread {

    private static final Logger LOGGER = LogManager.getLogger(RESTServer.class);
//    private String baseUrl;
    private int port;
    private String url;
    private static HttpServer server;

    public RESTServer(String url, int port){
        this.port = port;
//        this.baseUrl = url;
        this.url = url +":"+port;
    }

    public int getListeningPort(){
        return this.port;
    }

    public String getURL(){
        return this.url;
    }

    public void run() {
        LOGGER.info("Setting up REST server for sensor on port " + port);
//        URI baseUri = UriBuilder.fromUri(this.baseUrl).port(this.port).build();
        URI baseUri;
		try {
			baseUri = new URI(this.url);
			LOGGER.info("Configuring resources");
	        ResourceConfig rc = new ResourceConfig()
	                .register(RegistrationEndpoint.class);
	        LOGGER.info("Creating the HTTP server for url "+this.url+"...");
	        server = GrizzlyHttpServerFactory.createHttpServer(baseUri, rc);
            LOGGER.info("Starting REST server");
            server.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
