package fr.irit.melodi.sensor.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Model {
	private static Map<Integer, Model> instances;
	private List<String> listeningEndpoints;
	
	private Model(){
		this.listeningEndpoints = new ArrayList<>();
	}
	
	public static synchronized Model getInstance(Integer key){
		if(Model.instances == null){
			Model.instances = new HashMap<>();
		}
		if(Model.instances.get(key) == null){
			Model.instances.put(key, new Model());
	
		}
		return Model.instances.get(key);
	}
	
	public void addListener(String listeningEndpoint){
		this.listeningEndpoints.add(listeningEndpoint);
	}
	
	public List<String> getListeners(){
		return this.listeningEndpoints;
	}
	
	
}
