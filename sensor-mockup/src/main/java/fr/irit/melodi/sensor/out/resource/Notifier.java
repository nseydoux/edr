package fr.irit.melodi.sensor.out.resource;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;


public class Notifier {
	private static final Logger LOGGER = LogManager.getLogger(Notifier.class);

	public static Response notifyObservers(String endpoint, String observation){
		Client client = ClientBuilder.newClient(new ClientConfig());
		LOGGER.trace("Notifying observer "+endpoint);
		return client.target(endpoint)
	        .request(MediaType.TEXT_PLAIN)
	        .post(Entity.entity(observation, "text/csv"));
	}
}
