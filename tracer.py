import sys
import glob
import dateutil.parser
import numpy as np


if len(sys.argv) != 2:
    exit("usage : "+sys.argv[0]+" <logs folder>")

logs_folder_path = sys.argv[1]
trace=[]
for tracefile_path in glob.glob(logs_folder_path+"/*.trace"):
    with open(tracefile_path, "r") as tracefile:
        for line in tracefile:
            record = line.strip().split(",")
            trace.append({"time":record[0], "node":record[1], "event":record[2], "data":record[3], "target":record[4]})
trips = 0
negative_trips = 0
trip_times = []
for record_origin in [x for x in trace if x["event"] == "Sending data"]:
    for record_destination in [y for y in trace if y["event"] == "Received observation"]:
        if record_origin["target"] == record_destination["node"] and record_origin["data"] == record_destination["data"]:
            trips += 1
            #print(record_origin["time"]+", "+record_destination["time"])
            sent_time = dateutil.parser.parse(record_origin["time"])
            received_time = dateutil.parser.parse(record_destination["time"])
            trip_time = received_time - sent_time
            if(trip_time.total_seconds() > 0):
                trip_times.append(trip_time.total_seconds())
            else:
                negative_trips +=1

print(str(negative_trips)+" negative trips over "+str(trips)+" trips")

reasonings = {}
reasoning_times =[]

for record_origin in [x for x in trace if x["event"] == "Reasoning begins"]:
    for record_destination in [y for y in trace if y["event"] == "Reasoning ends"]:
        if(record_origin["node"] == record_destination["node"]) and (record_origin["data"] == record_destination["data"]):
            begin_time = dateutil.parser.parse(record_origin["time"])
            end_time = dateutil.parser.parse(record_destination["time"])
            reasoning_time = end_time - begin_time
            if record_origin["data"] in reasonings:
                if record_origin["target"] in reasonings[record_origin["data"]]:
                    reasonings[record_origin["data"]][record_origin["target"]]["time"] = reasoning_time.total_seconds()
            else:
                reasonings[record_origin["data"]] = {record_origin["target"]:{"time":reasoning_time.total_seconds()}}
            if reasoning_time.total_seconds() > 0:
                reasoning_times.append(reasoning_time.total_seconds())

for record in [x for x in trace if x["event"] == "New knowledge" or x["event"] == "No new knowledge"]:
    for obs in reasonings:
        if record["data"] == obs and record["target"] in reasonings[obs]:
            reasonings[obs][record["target"]]["deduction"] = record["event"]

print("Reasoning average : "+str(np.average(reasoning_times)))
print("Reasoning deviation : "+str(np.std(reasoning_times)))
print("Hop average : "+str(np.average(trip_times)))
print("Hop deviation : "+str(np.std(trip_times)))
# with open("test.log", "w") as logfile:
#     for obs in reasonings:
#         for rule in reasonings[obs]:
#             logfile.write(obs+", "+rule+":"+str(reasonings[obs][rule]["time"])+", "+reasonings[obs][rule]["deduction"]+"\n")
